<?php
if (! function_exists('profile'))
{
	function profile($param = '')
	{
		$CI =& get_instance();
		$where = array(
			'manajemenkerja_user_id' => $CI->session->userdata('id')
			);
		$nama = $CI->db->get_where('manajemenkerja_user',$where)->result_array();
		return $nama[0];
	}

}

function trace($data , $status = TRUE)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";

		if ($status) {
			die();
		}
	}

function tanggal($tanggal)
    {
        $tanggal_format = $tanggal;
        $tanggal_format = date('d-m-Y H:i', strtotime($tanggal_format));
        return $tanggal_format;
    }

function tanggalsaja($tanggal)
    {
        $tanggal_format = $tanggal;
        $tanggal_format = date('d-m-Y', strtotime($tanggal_format));
        return $tanggal_format;
    }