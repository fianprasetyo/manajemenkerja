<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kerja_model extends CI_Model {

	//public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	//menu Data Kerja  
	function data_list()
	{
		$res = $this->db->query(' SELECT manajemenkerja_user_id, manajemenkerja_user_nama, manajemenkerja_user_id, posisi_nama, kerja_judul, kerja_keterangan, kerja_id FROM manajemenkerja_kerja
			join manajemenkerja_user on manajemenkerja_user_id = kerja_manajemenkerjauserid
			join manajemenkerja_posisi on posisi_id = manajemenkerja_user_posisiid
			where kerja_delete = "0" ');
		return $res->result_array();
	}

	function tampil_data()
	{
		$res = $this->db->query(' SELECT manajemenkerja_user_id, manajemenkerja_user_nama, manajemenkerja_user_id, posisi_nama
			from manajemenkerja_user join manajemenkerja_posisi
			on posisi_id = manajemenkerja_user_posisiid
			where manajemenkerja_user_delete = "0" ');
		return $res->result_array();
	}

	//tampilkan data owner
	function tampil_data_owner($id)
	{
		$res = $this->db->query(' SELECT * FROM manajemenkerja_kerja_anggota join manajemenkerja_kerja on kerja_id = kerjaanggota_kerjaid where kerja_id = $id ');
		return $res->result_array();
	}

	function input_data($table, $data)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}

	function update_model($id, $data)
    {
        $this->db->where('kerja_id', $id);
        $this->db->update('manajemenkerja_kerja', $data);
    }

    function update_anggota_model($id, $data)
    {
        $this->db->where('kerjaanggota_kerjaid', $id);
        $this->db->update('manajemenkerja_kerja_anggota', $data);
    }

    function getById($id)
    {
    	$res = $this->db->query(" SELECT manajemenkerja_kerja.*, DATE_FORMAT(kerja_duedate, '%m/%d/%Y') as duedate, DATE_FORMAT(kerja_startdate, '%m/%d/%Y') as startdate  FROM `manajemenkerja_kerja` WHERE kerja_id='".$id."'
    		")->result_array()[0];

    	$member = $this->db->query(" SELECT * FROM manajemenkerja_kerja_anggota WHERE kerjaanggota_delete = '0' and kerjaanggota_kerjaid='".$id."'
    		")->result_array();
    	$res['kerja_member'] = $member;
    	return $res;
    }

    public function getKerjaById($id)
    {
    	$res = $this->db->query("
    		SELECT * FROM manajemenkerja_kerja WHERE kerja_id='".$id."'
    		")->result_array()[0];

    	$member = $this->db->query("
    		SELECT * FROM manajemenkerja_kerja_anggota WHERE kerjaanggota_kerjaid='".$id."'
    		")->result_array();
    	$res['kerja_member'] = $member;
    	return $res;
    }

    function getanggota()
    
    {
		$res = $this->db->query('SELECT manajemenkerja_user_id, manajemenkerja_user_nama, posisi_nama
			FROM manajemenkerja_user join manajemenkerja_posisi on manajemenkerja_user_posisiid = posisi_id and manajemenkerja_user_delete = "0" ');
		return $res->result_array();
	}
    

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */