<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class status_model extends CI_Model {

	//public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function tampil_data($table)
	{
		$res = $this->db->query("select*from manajemenkerja_status where status_delete = '0' ");
		return $res->result_array();
	}

	function input_data($table, $data)
	{
		$this->db->insert($table,$data);
	}

	function status_update_model($id, $data)
    {
        $this->db->where('status_id', $id);
        $this->db->update('manajemenkerja_status', $data);
    }

    function getById($id) {
        return $this->db->get_where('manajemenkerja_status', array('status_id' => $id))->row();
    }

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */