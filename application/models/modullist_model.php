<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modullist_model extends CI_Model {

	//public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	//menu Data Kerja  
	function data_list()
	{
		$res = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul_list
            join manajemenkerja_kerja_modul_list_duedate
            on kerjamodullist_id = kerjamodullistduedate_kerjamodullistid
            join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
            join manajemenkerja_status on status_id = kerjamodullist_statusid
            join manajemenkerja_user on manajemenkerja_user_id = kerjamodul_penanggungjawab
            where kerjamodullist_delete = "0"
            and kerjamodul_delete = "0"
            and kerjamodullistduedate_delete = "0"
            and kerjamodullistduedate_aktif = "1"
            ORDER BY kerjamodullist_id DESC ');
		return $res->result_array();
	}

	public function data_timeline($id)
	{
		$res = $this->db->query(' SELECT timeline_keterangan, timeline_judul, timeline_simbol, manajemenkerja_user_nama, DATE_FORMAT(timeline_createdate, "%d/%m/%Y") as tanggal, DATE_FORMAT(timeline_createdate, "%H:%i:%s") as waktu
            FROM manajemenkerja_timeline
            join manajemenkerja_user on timeline_manajemenkerja_user_id = manajemenkerja_user_id
            where  timeline_modullistid = "'.$id.'" order BY timeline_id DESC ');
        // trace($res);
		return $res->result_array();
	}

	function data_user_all()
	{
		$res = $this->db->query(' SELECT *
			from manajemenkerja_user join manajemenkerja_posisi
			on posisi_id = manajemenkerja_user_posisiid
			where manajemenkerja_user_delete = "0" ');
		return $res->result_array();
	}

	function data_modul_list($id)
	{
		$res = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul_list
            join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
            join manajemenkerja_kerja on kerja_id = kerjamodul_kerjaid
            join manajemenkerja_kerja_modul_list_duedate on kerjamodullist_id = kerjamodullistduedate_kerjamodullistid
            where kerjamodullist_delete = "0"
            and kerjamodullistduedate_delete = "0"
            and kerjamodullistduedate_aktif = "1"
            and kerjamodullist_id = "'.$id.'"
            order BY kerjamodullist_id ASC ')->result_array()[0];

            $member = $this->db->query(" SELECT *  FROM manajemenkerja_kerja_modul_list_anggota where kerjamodullistanggota_delete = '0' and kerjamodullistanggota_modullistid ='".$id."' ")->result_array();
            $res['list_member'] = $member;
		return $res;
	}

    // function data_modul_list_anggota($id)
    // {
    //     $res = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul_list
    //         join manajemenkerja_kerja_modul_list_anggota on kerjamodullist_id = kerjamodullistanggota_modullistid
    //         where kerjamodullist_delete = "0"
    //         and kerjamodullist_id = "'.$id.'"
    //         order BY kerjamodullist_id ASC ')->result_array()[0];
    //     return $res;
    // }

	function data_tim()
	{
		$res = $this->db->query(' SELECT manajemenkerja_user_id, manajemenkerja_user_nama, manajemenkerja_user_id, posisi_nama
			from manajemenkerja_user join manajemenkerja_posisi
			on posisi_id = manajemenkerja_user_posisiid
			where manajemenkerja_user_delete = "0" ');
		return $res->result_array();
	}

	//tampilkan data owner
	function tampil_data_owner($id)
	{
		$res = $this->db->query(' SELECT * FROM manajemenkerja_kerja_anggota join manajemenkerja_kerja on kerja_id = kerjaanggota_kerjaid where kerja_id = $id ');
		return $res->result_array();
	}

	function input_data($table, $data)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}

	function update_model($id, $data)
    {

        $this->db->where('kerjamodullist_id', $id);
        // trace($data);
        $this->db->update('manajemenkerja_kerja_modul_list', $data);
    }

    function update_modul_status_model($id, $data, $tabel, $kolomid)
    {
        $this->db->where($kolomid, $id);
        // trace($data);
        $res = $this->db->update($tabel, $data);
        return $res;
    }

    function data_modul_persentase($modulid)
    {
        $res = $this->db->query(" select (sum(if(kerjamodullist_statusid = '8',1,0)) / count(kerjamodullist_statusid)*100) as persen
        from manajemenkerja_kerja_modul_list 
        join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
        where kerjamodul_delete = '0'
        and kerjamodullist_delete = '0'
        and kerjamodul_id = '".$modulid."' ")->result_array()[0];
        return $res;
        // trace($res);
    }

    function update_persen_model($id, $data)
    {

        $this->db->where('kerjamodul_id', $id);
        // trace($data);
        $this->db->update('manajemenkerja_kerja_modul', $data);
    }

    function update_modul_programmer_model($id, $data)
    {
        $this->db->where('kerjamodullist_id', $id);
        // trace($id);
        $res = $this->db->update('manajemenkerja_kerja_modul_list', $data);
        return $res;
    }

    function update_list_model($id, $data)
    {
        $this->db->where('kerjamodullist_id', $id);
        $res = $this->db->update('manajemenkerja_kerja_modul_list', $data);
        return $res;
    }

    function timeline_model($table, $data)
    {
        $this->db->insert($table,$data);
		return $this->db->insert_id();
    }

    function update_anggota_model($id, $data)
    {
        $this->db->where('kerjaanggota_kerjaid', $id);
        $this->db->update('manajemenkerja_kerja_anggota', $data);
    }

    function hard_delete_model($id, $data, $tabel, $kolomid)
    {
        $this->db->where($kolomid, $id);
        $this->db->delete($tabel, $data);
    }

    function getById($id)
    {
    	$res = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul_list
            join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
            join manajemenkerja_penyelesaian on penyelesaian_id = kerjamodullist_penyelesaianid
            join manajemenkerja_user on manajemenkerja_user_id = kerjamodul_penanggungjawab
            where kerjamodullist_delete = '0' and kerjamodullist_id = '".$id."' ")->result_array()[0];

    	// $member = $this->db->query(" SELECT *  FROM manajemenkerja_kerja_modul_anggota where kerjamodulanggota_delete = '0' and kerjamodulanggota_modulid ='".$id."' ")->result_array();
    	// $res['kerja_member'] = $member;
    	return $res;
    }

    function status_modul()
    {
    	$res = $this->db->query(" SELECT * FROM manajemenkerja_status where status_delete = '0' ");
    	return $res->result_array();
    }

    function pencapaian($id)
    {
    	$res = $this->db->query(" SELECT (sum(kerjamodullist_status)/COUNT(kerjamodullist_status)*100) as pencapaian FROM manajemenkerja_kerja_modul
    		join manajemenkerja_kerja_modul_list on kerjamodul_id = kerjamodullist_kerjamodulid
    		where kerjamodul_id = '".$id."' ")->result_array()[0];

    	return $res;
    }

    public function getKerjaById($id)
    {
    	$res = $this->db->query("
    		SELECT * FROM manajemenkerja_kerja WHERE kerja_id='".$id."'
    		")->result_array()[0];

    	$member = $this->db->query("
    		SELECT * FROM manajemenkerja_kerja_modul_anggota WHERE kerjamodulanggota_modulid ='".$id."'
    		")->result_array();
    	$res['kerja_member'] = $member;
    	return $res;
    }

    function getmodul($id = "")
    {
        if (empty($id))
        {
            $strFilter = "";
        }
        else
        {
            $strFilter = " and kerjamodul_kerjaid = '".$id."' ";
        }
		$res = $this->db->query("SELECT * FROM manajemenkerja_kerja_modul
            where kerjamodul_delete = '0'
            and kerjamodul_statusid not in ('8')" .$strFilter);
		return $res->result_array();
	}

    function getkerja()
    {
        $res = $this->db->query('SELECT * FROM manajemenkerja_kerja where kerja_delete = "0" ');
        return $res->result_array();
    }

	function getuser()
    {
		$res = $this->db->query('SELECT * FROM manajemenkerja_user where manajemenkerja_user_delete = "0" ');
		return $res->result_array();
	}

    function getalldata($tabel, $kolomdelete)
    {
        $res = $this->db->query("SELECT * FROM $tabel where $kolomdelete = '0' ");
        return $res->result_array();
    }
    

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */