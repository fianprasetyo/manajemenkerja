<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model {

	//public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	function tampil_data($table)
	{
		$res = $this->db->query("select*from manajemenkerja_user where manajemenkerja_user_delete = '0' ");
		return $res->result_array();
	}

	function input_data($table, $data)
	{
		$this->db->insert($table,$data);
	}

	function user_update_model($id, $data)
    {
        $this->db->where('manajemenkerja_user_id', $id);
        $this->db->update('manajemenkerja_user', $data);
    }

    function getById($id) {
        return $this->db->get_where('manajemenkerja_user', array('manajemenkerja_user_id' => $id))->row();
    }

    function data_user($id)
    {
        $res = $this->db->query(" SELECT*From manajemenkerja_user join manajemenkerja_posisi on posisi_id = manajemenkerja_user_posisiid where manajemenkerja_user_delete = '0' and manajemenkerja_user_id = '".$id."' ")->result_array()[0];
		return $res;
    }

    function data_modul($id)
    {
        $res = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul
        	where kerjamodul_delete = '0' and kerjamodul_penanggungjawab = '".$id."' ");
		return $res->result_array();
    }
    function data_modulid($id)
    {
        $res = $this->db->query(" SELECT kerjamodul_id FROM manajemenkerja_kerja_modul
        	where kerjamodul_delete = '0' and kerjamodul_penanggungjawab = '".$id."' ");
		return $res->result_array();
    }

    function data_list_duedate($id)
    {
        $res = $this->db->query(" SELECT kerjamodul_id FROM manajemenkerja_kerja_modul
            where kerjamodul_delete = '0' and kerjamodul_penanggungjawab = '".$id."' ");
        return $res->result_array();
    }

    function data_modullist_done($id, $idmodul)
    {
        $res = $this->db->query(" select count(kerjamodullist_statusid) as total,
sum(if(kerjamodullist_statusid = '8',1,0)) as selesai,
(sum(if(kerjamodullist_statusid = '8',1,0)) / count(kerjamodullist_statusid)*100) as totals
 from manajemenkerja_kerja_modul_list 
join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
where kerjamodul_penanggungjawab = '13' and kerjamodul_id = '24'
 ");
		return $res->result_array();
    }

    function data_modullist_notin_done($id, $idmodul)
    {
        $res = $this->db->query(" SELECT COUNT(kerjamodullist_statusid)
        	FROM manajemenkerja_kerja_modul
        	JOIN manajemenkerja_kerja_modul_list on kerjamodul_id = kerjamodullist_kerjamodulid
        	where kerjamodul_delete = '0' and kerjamodul_penanggungjawab = '".$id."'
        	and kerjamodul_id = '".$idmodul."' and kerjamodullist_statusid not in('8') ");
		return $res->result_array();
    }


    function data_list($id, $ex_rentangwaktu_format_baru = null, $ex_akhir_rentangwaktu_format_baru = null, $jam = '11:59:59')
    {;
        if(is_null($ex_rentangwaktu_format_baru))
        {
            $res = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul_list
            join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
            where kerjamodullist_delete = '0'
            and kerjamodul_delete = '0'
            and kerjamodullist_statusid in ('8')
            and kerjamodul_penanggungjawab = '".$id."' ")->result_array();
            foreach ($res as $key => $value)
            {
                $list_duedate = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul_list_duedate where kerjamodullistduedate_kerjamodullistid = '".$value['kerjamodullist_id']."'
                ")->result_array();
                $res[$key]['data_duedate'] = $list_duedate;

                $selisih = $this->db->query(" select
                    MAX(DATE_FORMAT(kerjamodullistduedate_duedate, '%d-%m-%Y'))-MIN(DATE_FORMAT(kerjamodullistduedate_duedate,'%d-%m-%Y')) as selisih
                    from manajemenkerja_kerja_modul_list_duedate
                    where kerjamodullistduedate_kerjamodullistid = '".$value['kerjamodullist_id']."' ")->result_array();
                $res[$key]['selisih'] = $selisih;
        }
            
            return $res;
        }
        else
        {
            $res = $this->db->query(" (SELECT * FROM manajemenkerja_kerja_modul_list
            join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
            where kerjamodullist_delete = '0'
            and kerjamodul_delete = '0'
            and kerjamodullist_statusid in ('8')
            and kerjamodullist_startdate >= ' ".$ex_rentangwaktu_format_baru." '
            and kerjamodullist_finish <= ' ".$ex_akhir_rentangwaktu_format_baru.".".$jam." '
            and kerjamodul_penanggungjawab = '".$id."')
            union
            (SELECT * FROM manajemenkerja_kerja_modul_list
            join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
            where kerjamodullist_delete = '0'
            and kerjamodullist_statusid in ('8')
            and kerjamodullist_startdate <= ' ".$ex_rentangwaktu_format_baru." '
            and kerjamodullist_finish <= ' ".$ex_akhir_rentangwaktu_format_baru.".".$jam." '
            and kerjamodul_penanggungjawab = '".$id."') ")->result_array();
            foreach ($res as $key => $value)
            {
                $list_duedate = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul_list_duedate where kerjamodullistduedate_kerjamodullistid = '".$value['kerjamodullist_id']."'
                ")->result_array();
                $res[$key]['data_duedate'] = $list_duedate;

                $selisih = $this->db->query(" select
                    MAX(DATE_FORMAT(kerjamodullistduedate_duedate, '%d-%m-%Y'))-MIN(DATE_FORMAT(kerjamodullistduedate_duedate,'%d-%m-%Y')) as selisih
                    from manajemenkerja_kerja_modul_list_duedate
                    where kerjamodullistduedate_kerjamodullistid = '".$value['kerjamodullist_id']."' ")->result_array();
                $res[$key]['selisih'] = $selisih;
            }
            return $res;
        }
    }

    function data_list_terlambat($id, $ex_rentangwaktu_format_baru = null)
    {
        if(is_null($ex_rentangwaktu_format_baru))
        {
            $res = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul_list
                join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
                where kerjamodullist_delete = '0'
                and kerjamodul_delete = '0'
                and kerjamodullist_statusid not in ('8')
                and kerjamodul_penanggungjawab = '".$id."' ")->result_array();
                foreach ($res as $key => $value)
                {
                    $list_duedate = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul_list_duedate
                        where kerjamodullistduedate_kerjamodullistid = '".$value['kerjamodullist_id']."'
                    ")->result_array();
                    $res[$key]['data_duedate'] = $list_duedate;

                    $selisih = $this->db->query(" select
                        MAX(DATE_FORMAT(kerjamodullistduedate_duedate, '%d-%m-%Y'))-MIN(DATE_FORMAT(kerjamodullistduedate_duedate,'%d-%m-%Y')) as selisih
                        from manajemenkerja_kerja_modul_list_duedate
                        where kerjamodullistduedate_kerjamodullistid = '".$value['kerjamodullist_id']."' ")->result_array();

                    $res[$key]['selisih'] = $selisih;
                }
                return $res;
        }
        else
        {
            $res = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul_list
                join manajemenkerja_kerja_modul on kerjamodul_id = kerjamodullist_kerjamodulid
                where kerjamodullist_delete = '0'
                and kerjamodul_delete = '0'
                and kerjamodullist_statusid not in ('8')
                and kerjamodullist_startdate >= '".$ex_rentangwaktu_format_baru."'
                and kerjamodul_penanggungjawab = '".$id."' ")->result_array();
                foreach ($res as $key => $value)
                {
                    $list_duedate = $this->db->query(" SELECT * FROM manajemenkerja_kerja_modul_list_duedate
                        where kerjamodullistduedate_kerjamodullistid = '".$value['kerjamodullist_id']."'
                    ")->result_array();
                    $res[$key]['data_duedate'] = $list_duedate;
                }
                return $res;
        }
    }

    

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */