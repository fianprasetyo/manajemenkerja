<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class modul_model extends CI_Model {

	//public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	//menu Data Kerja  
	function data_list()
	{
		$res = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul
            join manajemenkerja_status on status_id = kerjamodul_statusid
            join manajemenkerja_kerja on kerja_id = kerjamodul_kerjaid
            join manajemenkerja_user on manajemenkerja_user_id = kerjamodul_penanggungjawab
            where kerjamodul_delete = "0" ORDER BY kerjamodul_id DESC ');
		return $res->result_array();
	}

	public function data_timeline($id)
	{
		$res = $this->db->query(' SELECT timeline_keterangan, timeline_judul, timeline_simbol, manajemenkerja_user_nama, DATE_FORMAT(timeline_createdate, "%d/%m/%Y") as tanggal, DATE_FORMAT(timeline_createdate, "%H:%i:%s") as waktu FROM manajemenkerja_timeline join manajemenkerja_user on timeline_manajemenkerja_user_id = manajemenkerja_user_id and timeline_modul = "'.$id.'" order BY timeline_id DESC ');
		return $res->result_array();
	}

	function data_user_all()
	{
		$res = $this->db->query(' SELECT manajemenkerja_user_id, manajemenkerja_user_nama, manajemenkerja_user_id, posisi_nama
			from manajemenkerja_user join manajemenkerja_posisi
			on posisi_id = manajemenkerja_user_posisiid
			where manajemenkerja_user_delete = "0" ');
		return $res->result_array();
	}

	function data_modul_list($id)
	{
		$res = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul_list
            join manajemenkerja_kerja_modul_list_duedate
            on kerjamodullist_id = kerjamodullistduedate_kerjamodullistid
            where kerjamodullist_delete = "0"
            and kerjamodullistduedate_delete = "0"
            and kerjamodullistduedate_aktif = "1"
            and kerjamodullist_kerjamodulid = "'.$id.'" order BY kerjamodullist_id ASC ');
		return $res->result_array();
	}

	function data_tim()
	{
		$res = $this->db->query(' SELECT manajemenkerja_user_id, manajemenkerja_user_nama, manajemenkerja_user_id, posisi_nama
			from manajemenkerja_user join manajemenkerja_posisi
			on posisi_id = manajemenkerja_user_posisiid
			where manajemenkerja_user_delete = "0" ');
		return $res->result_array();
	}

	//tampilkan data owner
	function tampil_data_owner($id)
	{
		$res = $this->db->query(' SELECT * FROM manajemenkerja_kerja_anggota join manajemenkerja_kerja on kerja_id = kerjaanggota_kerjaid where kerja_id = $id ');
		return $res->result_array();
	}

	function input_data($table, $data)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}

	function update_model($id, $data)
    {

        $this->db->where('kerjamodul_id', $id);
        // trace($data);
        $this->db->update('manajemenkerja_kerja_modul', $data);
    }

    function update_model_list($id, $data, $tabel, $kolomid)
    {

        $this->db->where($kolomid, $id);
        // trace($data);
        $this->db->update($tabel, $data);
    }

    function update_modul_status_model($id, $data)
    {
        $this->db->where('kerjamodul_id', $id);
        // trace($data);
        $res = $this->db->update('manajemenkerja_kerja_modul', $data);
        return $res;
    }

    function update_list_model($id, $data)
    {
        $this->db->where('kerjamodullist_id', $id);
        $res = $this->db->update('manajemenkerja_kerja_modul_list', $data);
        return $res;
    }

    function timeline_model($table, $data)
    {
        $this->db->insert($table,$data);
		return $this->db->insert_id();
    }

    function update_anggota_model($id, $data)
    {
        $this->db->where('kerjaanggota_kerjaid', $id);
        $this->db->update('manajemenkerja_kerja_anggota', $data);
    }

    function hard_delete_model($id, $data, $tabel, $kolomid)
    {
        $this->db->where($kolomid, $id);
        $this->db->delete($tabel, $data);
    }

    function getById($id)
    {
    	$res = $this->db->query(" SELECT manajemenkerja_kerja_modul.*,
    		manajemenkerja_kerja.*, manajemenkerja_user.*,
    		DATE_FORMAT(kerjamodul_startdate, '%m/%d/%Y') as startdate,
    		DATE_FORMAT(kerjamodul_duedate, '%m/%d/%Y') as duedate
    		FROM manajemenkerja_kerja_modul
    		join manajemenkerja_kerja on kerja_id = kerjamodul_kerjaid
    		JOIN manajemenkerja_user on manajemenkerja_user_id = kerjamodul_penanggungjawab
    		where kerjamodul_id = '".$id."' ")->result_array()[0];

    	$member = $this->db->query(" SELECT *  FROM manajemenkerja_kerja_modul_anggota where kerjamodulanggota_delete = '0' and kerjamodulanggota_modulid ='".$id."' ")->result_array();
    	$res['kerja_member'] = $member;
    	return $res;
    }

    function status_modul()
    {
    	$res = $this->db->query(" SELECT * FROM manajemenkerja_status where status_delete = '0' ");
    	return $res->result_array();
    }

    function pencapaian($id)
    {
    	$res = $this->db->query(" SELECT kerjamodullist_penyelesaianid FROM manajemenkerja_kerja_modul
            join manajemenkerja_kerja_modul_list on kerjamodul_id = kerjamodullist_kerjamodulid
            where kerjamodul_id = '".$id."' ");
        return $res->result_array();
    }

    function pencapaianselesai($id)
    {
        $res = $this->db->query(" sELECT kerjamodullist_penyelesaianid FROM manajemenkerja_kerja_modul
            join manajemenkerja_kerja_modul_list on kerjamodul_id = kerjamodullist_kerjamodulid
            where kerjamodullist_penyelesaianid = '2' and kerjamodul_id = '".$id."' ");
        return $res->result_array();
    }

    public function getKerjaById($id)
    {
    	$res = $this->db->query("
    		SELECT * FROM manajemenkerja_kerja WHERE kerja_id='".$id."'
    		")->result_array()[0];

    	$member = $this->db->query("
    		SELECT * FROM manajemenkerja_kerja_modul_anggota WHERE kerjamodulanggota_modulid ='".$id."'
    		")->result_array();
    	$res['kerja_member'] = $member;
    	return $res;
    }

    function getkerja()
    {
		$res = $this->db->query('SELECT * FROM manajemenkerja_kerja where kerja_delete = "0" ');
		return $res->result_array();
	}

	function getuser()
    {
		$res = $this->db->query('SELECT * FROM manajemenkerja_user where manajemenkerja_user_delete = "0" ');
		return $res->result_array();
	}
    

}

/* End of file user_model.php */
/* Location: ./application/models/user_model.php */