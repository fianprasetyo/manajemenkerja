<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kerja extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('validasi_login');
		$this->load->model('kerja_model');
	}

	public function index()
	{
		$this->load->model('kerja_model');
		$data = $this->kerja_model->data_list();
		$data = array('data' => $data);
		$this->load->view('kerja/kerja_view', $data);
	}

	public function tambah()
	{
        $data['data'] = $this->kerja_model->getanggota();
    	$this->load->view('kerja/kerja_tambah_view', $data);
	}


    public function insert()
    {
    	$this->form_validation->set_rules('judul', 'judul', 'required');
    	$this->form_validation->set_rules('startdate', 'start date', 'required');
    	$this->form_validation->set_rules('duedate', 'due date', 'required');
    	$this->form_validation->set_rules('anggota[]', 'anggota', 'required');

    	if ($this->form_validation->run() == FALSE)
    	{
    		$this->session->set_flashdata('error', validation_errors());
    		redirect('kerja/tambah');
		}
        else
        {
            $this->load->view('kerja/kerja_view');
        }

    	$this->load->model('kerja_model');

        $duedate = DateTime::createFromFormat('d/m/Y', $this->input->post('duedate'));
        $startdate = DateTime::createFromFormat('d/m/Y', $this->input->post('startdate'));

    	$data = array(
        'kerja_judul' => $this->input->post('judul'),
        'kerja_keterangan' => $this->input->post('keterangan'),
        'kerja_manajemenkerjauserid' => $this->input->post('owner'),
        'kerja_delete' => '0',
        'kerja_createby' => profile()['manajemenkerja_user_id'],
        'kerja_startdate' => $startdate->format('Y-m-d H:i:s'),
        'kerja_duedate' => $duedate->format('Y-m-d H:i:s')
         );
        $res = $this->kerja_model->input_data('manajemenkerja_kerja', $data);

        $dataanggota = array();

        for ($i=0; $i < count($this->input->post('anggota')) ; $i++)
        { 
            $dataanggota = array(
                'kerjaanggota_kerjaid' => $res,
                'kerjaanggota_delete' => '0',
                'kerjaanggota_userid' => $this->input->post('anggota')[$i]
                );
            $resanggota = $this->kerja_model->input_data('manajemenkerja_kerja_anggota', $dataanggota);
        }

    	redirect(site_url("kerja"),'tambah');
    }

    //menampilkan data yang terpilih
    public function update_data($id)
    {
        $data['kerja'] = $this->kerja_model->getById($id);
        $data['owner'] = $this->kerja_model->tampil_data();
        $data['tim'] = $this->kerja_model->tampil_data();

        $tim = array();
        
        foreach ($data['kerja']['kerja_member'] as $rows)
        {
            array_push($tim, $rows['kerjaanggota_userid']);
        }
        $data['kerja']['kerja_member_id'] = $tim;

        $this->load->view('kerja/kerja_edit_view', $data);
    }

    public function user_detail_data($id)
    {
        $data['hasil'] = $this->user_model->getById($id);
        $this->load->view('user/user_detail_view', $data);
 
    }

    public function update_simpan($id)
    {
        $duedate = DateTime::createFromFormat('m/d/Y', $this->input->post('duedate'));
        $startdate = DateTime::createFromFormat('m/d/Y', $this->input->post('startdate'));
        $owner = $this->input->post('owner');
        $anggota = $this->input->post('anggota[]');

        $data = array(
        'kerja_judul' => $this->input->post('judul'),
        'kerja_keterangan' => $this->input->post('keterangan'),
        'kerja_manajemenkerjauserid' => $this->input->post('owner'),
        'kerja_startdate' => $startdate->format('Y-m-d H:i:s'),
        'kerja_duedate' => $duedate->format('Y-m-d H:i:s')
         );
		$this->kerja_model->update_model($id,$data);

        $dataanggota = array(
            'kerjaanggota_delete' => '1'
            );
        $this->kerja_model->update_anggota_model($id, $dataanggota);

        for ($i=0; $i < count($this->input->post('anggota')) ; $i++)
        { 
            $dataanggota = array(
                'kerjaanggota_kerjaid' => $id,
                'kerjaanggota_delete' => '0',
                'kerjaanggota_userid' => $this->input->post('anggota')[$i]
                );
            $resanggota = $this->kerja_model->input_data('manajemenkerja_kerja_anggota', $dataanggota);
        }
        
		redirect(site_url("kerja"),'refresh');
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $data = array (
                'kerja_delete' => '1'
            );

        $res = $this->kerja_model->update_model($id,$data);
        $data = array(
            'status' => 1,
            'message' => 'Berhasil hapus data karyawan'
        );
        die(json_encode($data));
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */