<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Status extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('validasi_login');
		$this->load->model('status_model');
	}

	public function index()
	{
		$this->load->model('status_model');
		$data = $this->status_model->tampil_data('manajemenkerja_status');
		$data = array('data' => $data);
		$this->load->view('status/status_view', $data);
	}

	public function status_tambah()
	{
    	$this->load->view('status/status_tambah_view');
	}


    public function status_insert()
    {
    	$this->form_validation->set_rules('nama', 'nama', 'required');

    	if ($this->form_validation->run() == FALSE)
    	{
    		$this->session->set_flashdata('error', validation_errors());
    		redirect('status/status_tambah');
		}
        else
        {
            $this->load->view('status/status_tambah_view');
        }

    	$this->load->model('status_model');

    	$data = array(
        'status_nama' => $this->input->post('nama'),
        'status_keterangan' => $this->input->post('keterangan'),
        'status_delete' => '0'
         );

    	$res = $this->status_model->input_data('manajemenkerja_status', $data);
    	redirect(site_url("status"),'refresh');
    }

    public function status_update_data($id)
    {
        $data['hasil'] = $this->status_model->getById($id);
        $this->load->view('status/status_edit_view', $data);
 
    }

    public function status_update_simpan()
    {

    	$id= $this->input->post('id');

    	$nama  = $this->input->post('nama');
        $keterangan = $this->input->post('keterangan');

    	$data = array (
            'status_nama' => $nama,
            'status_keterangan'  => $keterangan
        );

		$this->status_model->status_update_model($id,$data);
		redirect(site_url("status"),'refresh');
    }

    public function status_delete()
    {
        $id = $this->input->post('id');
    	$data = array (
	            'status_delete' => '1'
        	);

    	$res = $this->status_model->status_update_model($id,$data);
		$data = array(
            'status' => 1,
            'message' => 'Berhasil hapus data karyawan'
        );
        die(json_encode($data));
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */