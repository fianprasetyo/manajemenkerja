<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modul extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('validasi_login');
        $this->load->model('modul_model');
		$this->load->model('modullist_model');
	}

	public function index()
	{
		$this->load->model('modul_model');
		$data = $this->modul_model->data_list();
		$data = array('data' => $data);
		$this->load->view('modul/modul_view', $data);
	}

	public function tambah()
	{
        $data['data'] = $this->modul_model->getkerja();
        $data['user'] = $this->modul_model->getuser();
        $data['url'] = 'insert';
    	$this->load->view('modul/tambah_view', $data);
	}


    public function insert()
    {
        $this->form_validation->set_rules('kerja', 'kerja', 'required');
        $this->form_validation->set_rules('judul', 'judul', 'required');
    	$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
        $this->form_validation->set_rules('duedatemodul', 'duedatemodul', 'required');
    	$this->form_validation->set_rules('penanggungjawab', 'penanggung jawab', 'required');
        $this->form_validation->set_rules('tester', 'tester', 'required');
    	// $this->form_validation->set_rules('list[]', 'list', 'required');
    	if ($this->form_validation->run() == FALSE)
    	{
    		$this->session->set_flashdata('error', validation_errors());
    		redirect('modul/tambah');
		}
        else
        {
            $this->load->view('modul/modul_view');
        }

    	$this->load->model('modul_model');

        $duedatemodul = DateTime::createFromFormat('m/d/Y', $this->input->post('duedatemodul'));

    	$data = array(
        'kerjamodul_kerjaid' => $this->input->post('kerja'),
        'kerjamodul_judul' => $this->input->post('judul'),
        'kerjamodul_keterangan' => $this->input->post('keterangan'),
        'kerjamodul_duedate' => $duedatemodul->format('Y-m-d H:i:s'),
        'kerjamodul_createby' => profile()['manajemenkerja_user_id'],
        'kerjamodul_penanggungjawab' => $this->input->post('penanggungjawab'),
        'kerjamodul_tester' => $this->input->post('tester'),
        'kerjamodul_statusid' => '1',
        'kerjamodul_persen' => '0',
        'kerjamodul_delete' => '0'
         );
        $res = $this->modul_model->input_data('manajemenkerja_kerja_modul', $data);
        // trace($res);

        // for ($i=0; $i < count($this->input->post('list')) ; $i++)
        // { 
        //     $datalist = array(
        //         'kerjamodullist_kerjamodulid' => $res,
        //         'kerjamodullist_judul' => $this->input->post('list')[$i],
        //         'kerjamodullist_delete' => '0',
        //         'kerjamodullist_statusid' => '1',
        //         'kerjamodullist_penyelesaianid' => '1',
        //         'kerjamodullist_createby' => profile()['manajemenkerja_user_id']
        //         );
        //     $reslist = $this->modul_model->input_data('manajemenkerja_kerja_modul_list', $datalist);
       
        //     $duedate = DateTime::createFromFormat('m/d/Y', $this->input->post('duedate')[$i]);
        //     $dataduedate = array(
        //         'kerjamodullistduedate_kerjamodullistid' => $reslist,
        //         'kerjamodullistduedate_duedate' => $duedatemodul->format('Y-m-d H:i:s'),
        //         'kerjamodullistduedate_createby' => profile()['manajemenkerja_user_id'],
        //         'kerjamodullistduedate_delete' => '0',
        //         'kerjamodullistduedate_aktif' => '1'
        //         );
        //     $resduedate = $this->modul_model->input_data('manajemenkerja_kerja_modul_list_duedate', $dataduedate);
        // }

    	redirect(site_url("modul"),'refresh');
    }

    //menampilkan data yang terpilih
    public function update_data($id)
    {
        $data['modul']  = $this->modul_model->getById($id);
        $data['user']   = $this->modul_model->data_user_all();
        $data['tim']    = $this->modul_model->data_tim();
        $data['list']   = $this->modul_model->data_modul_list($id);

        $tim = array();
        
        foreach ($data['modul']['kerja_member'] as $rows)
        {
            array_push($tim, $rows['kerjamodulanggota_manajemenkerja_user_id']);
        }

        $data['modul']['modul_member_id'] = $tim;
        $this->load->view('modul/edit_view', $data);
    }

    public function update_simpan($id)
    {
        $duedatemodul = DateTime::createFromFormat('m/d/Y', $this->input->post('duedatemodul'));
        $owner = $this->input->post('owner');
        $anggota = $this->input->post('anggota[]');

        $data = array(
        'kerjamodul_judul' => $this->input->post('judul'),
        'kerjamodul_keterangan' => $this->input->post('keterangan'),
        'kerjamodul_penanggungjawab' => $this->input->post('penanggungjawab'),
        'kerjamodul_tester' => $this->input->post('tester'),
        'kerjamodul_duedate' => $duedatemodul->format('Y-m-d H:i:s')
         );

        $res = $this->modul_model->update_model($id, $data);

        $datalist = array(
            'kerjamodullist_kerjamodulid' => $id
            );

        //update list
        for ($i=0; $i < count($this->input->post('list')) ; $i++)
        { 
            $datalist = array(
                'kerjamodullist_judul' => $this->input->post('list')[$i],
                );
                $modullist_id = $this->input->post('modullist_id')[$i];
                $reslist = $this->modul_model->update_model_list($modullist_id, $datalist, 'manajemenkerja_kerja_modul_list', 'kerjamodullist_id');

            // $duedate = DateTime::createFromFormat('m/d/Y', $this->input->post('duedate')[$i]);
            // $dataduedate = array(
            //     'kerjamodullistduedate_kerjamodullistid' => $reslist,
            //     'kerjamodullistduedate_duedate' => $duedatemodul->format('Y-m-d H:i:s'),
            //     'kerjamodullistduedate_createby' => profile()['manajemenkerja_user_id'],
            //     'kerjamodullistduedate_delete' => '0',
            //     'kerjamodullistduedate_aktif' => '1'
            //     );
            // $resduedate = $this->modul_model->input_data('manajemenkerja_kerja_modul_list_duedate', $dataduedate);
        }

        // $persentase = $this->modullist_model->data_modul_persentase($id);
        // $datapersen = array (
        //         'kerjamodul_persen' => ($persentase['persen'])
        //     );
        // $respersen = $this->modullist_model->update_persen_model($id,$datapersen);

        redirect(site_url("modul"),'refresh');
    }

    public function detail_data($id)
    {
        $data['pencapaian'] = $this->modul_model->pencapaian($id);
        $data['pencapaianselesai'] = $this->modul_model->pencapaianselesai($id);
        $data['status_modul'] = $this->modul_model->status_modul();
        $data['modul']      = $this->modul_model->getById($id);
        $data['judul']      = 'Modul List';
        $data['user']       = $this->modul_model->data_user_all();
        $data['tim']        = $this->modul_model->data_tim();
        $data['list']       = $this->modul_model->data_modul_list($id);
        $data['timeline']   = $this->modul_model->data_timeline($id);

        $tim = array();
        
        foreach ($data['modul']['kerja_member'] as $rows)
        {
            array_push($tim, $rows['kerjamodulanggota_manajemenkerja_user_id']);
        }

        $data['modul']['modul_member_id'] = $tim;
        $this->load->view('modul/detail_view', $data);
    }


    public function update_simpan_list()
    {
        foreach ($this->input->post('list') as $value)
        {
            $data = array(
            'kerjamodullist_status' => "1"
             );
            trace($data, FALSE);
            $this->modul_model->update_list_model($value,$data);
        }
        die();
		redirect(site_url("modul"),'refresh');
    }

    public function update_status_modul()
    {
        $status = $this->input->post('status');
        $id = $this->input->post('modulid');
        $statusnama = $this->input->post('statusnama');

        $data = array(
            'kerjamodul_statusid' => $status
            );
        $res = $this->modul_model->update_modul_status_model($id,$data);

        $data_timeline = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => 'Mengubah',
            'timeline_keterangan' =>  'mengubah status menjadi '.$statusnama,
            'timeline_modul' => $id,
            'timeline_simbol' => 'fa-exchange'
            );
        $res_timeline = $this->modul_model->timeline_model('manajemenkerja_timeline',$data_timeline);

        if ($res && $res_timeline)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));

    }

    public function komentar_insert()
    {
        $id = $this->input->post('id');
        $result_list = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul_list where kerjamodullist_id = "'.$id.'" ');

        $data = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => 'berkomentar',
            'timeline_keterangan' => $this->input->post('komentar'),
            'timeline_modul' => $id,
            'timeline_simbol' => 'fa-wechat'
            );

        $res = $this->modul_model->timeline_model('manajemenkerja_timeline',$data);
        if ($res)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));
    }

    public function updateListById()
    {
        $id = $this->input->post('id');
        if ($this->input->post('status') == "tambah")
        {
            $valueStatus = '1';
        }
        else
        {
            $valueStatus = '0';
        }
        $data = array(
            'kerjamodullist_status' => $valueStatus
            );
        $res = $this->modul_model->update_list_model($id,$data);

        $result_list = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul_list where kerjamodullist_id = "'.$id.'" ');
        $modulId = $result_list->row()->kerjamodullist_kerjamodulid;
        $keterangan = $result_list->row()->kerjamodullist_judul;

        if ($valueStatus == '1')
        {
            $judul ="Centang";
            $simbol = "fa-check";
        }
        else
        {
            $judul ="Urungkan";
            $simbol = "fa-square-o";
        }
            $data_timeline = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => $judul,
            'timeline_keterangan' =>  $keterangan,
            'timeline_modul' => $modulId,
            'timeline_simbol' => $simbol
            );
            $res = $this->modul_model->timeline_model('manajemenkerja_timeline',$data_timeline);
        
        if ($res)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));

    }

    public function delete()
    {
        $id = $this->input->post('id');
        $data = array (
                'kerjamodul_delete' => '1'
            );

        $res = $this->modul_model->update_model($id,$data);
        $data = array(
            'status' => 1,
            'message' => 'Berhasil hapus data karyawan'
        );
        die(json_encode($data));
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */