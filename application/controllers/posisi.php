<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posisi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('validasi_login');
		$this->load->model('posisi_model');
	}

	public function index()
	{
		$this->load->model('posisi_model');
		$data = $this->posisi_model->tampil_data('manajemenkerja_posisi');
		$data = array('data' => $data);
		$this->load->view('posisi/posisi_view', $data);
	}

	public function posisi_tambah()
	{
    	$this->load->view('posisi/posisi_tambah_view');
	}


    public function posisi_insert()
    {
    	$this->form_validation->set_rules('namaposisi', 'nama posisi', 'required');

    	if ($this->form_validation->run() == FALSE)
    	{
    		$this->session->set_flashdata('error', validation_errors());
    		redirect('posisi/posisi_tambah');
		}
        else
        {
            $this->load->view('posisi/posisi_tambah_view');
        }

    	$this->load->model('posisi_model');

    	$data = array(
        'posisi_nama' => $this->input->post('namaposisi'),
        'posisi_keterangan' => $this->input->post('keterangan'),
        'posisi_delete' => '0',
        'posisi_createby' => profile()['manajemenkerja_user_id']
         );
    	// echo "<pre>";
    	// print_r($data);
    	// echo "</pre>";
    	// die();
    	$res = $this->posisi_model->input_data('manajemenkerja_posisi', $data);
    	redirect(site_url("posisi"),'refresh');
    }

    public function posisi_update_data($id)
    {
        $data['hasil'] = $this->posisi_model->getById($id);
        $this->load->view('posisi/posisi_edit_view', $data);
 
    }

    public function posisi_update_simpan($id)
    {
    	$data = array (
            'posisi_nama' =>$this->input->post('nama'),
            'posisi_keterangan'  => $this->input->post('keterangan')
        );

		$this->posisi_model->posisi_update_model($id,$data);
		redirect(site_url("posisi"),'refresh');
    }

    public function posisi_delete()
    {
        $id = $this->input->post('id');
    	$data = array (
	            'posisi_delete' => '1'
        	);

    	$res = $this->posisi_model->posisi_update_model($id,$data);
        $data = array(
            'status' => 1,
            'message' => 'Berhasil hapus data karyawan'
        );
        die(json_encode($data));
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */