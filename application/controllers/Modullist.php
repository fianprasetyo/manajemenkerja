<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modullist extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('validasi_login');
        $this->load->model('modullist_model');
		$this->load->model('modul_model');
	}

	public function index()
	{
		$this->load->model('modullist_model');
		$data = $this->modullist_model->data_list();
		$data = array('data' => $data);
		$this->load->view('modullist/view', $data);
	}

	public function tambah()
	{
        $data['kerja']      = $this->modullist_model->getkerja();
        $data['listjenis']  = $this->modullist_model->getalldata('manajemenkerja_kerja_modul_list_jenis', 'kerjamodullistjenis_delete');
        $data['user']       = $this->modullist_model->getuser();
        $data['url']        = 'insert';
    	$this->load->view('modullist/tambah_view', $data);
	}

    function getmodul()
    {
        $id = $this->input->post('id');
        $data['data']   = $this->modullist_model->getmodul($id);
        echo "<option value=''></option>";
        foreach ($data['data'] as $rows) {
            echo "<option value='".$rows['kerjamodul_id']."'>".$rows['kerjamodul_judul']."</option>";
        }
        // trace($this->db->last_query());
    }

    public function insert()
    {
        $this->form_validation->set_rules('modul', 'modul', 'required');
        $this->form_validation->set_rules('list', 'list', 'required');
        $this->form_validation->set_rules('listjenis', 'jenis list', 'required');
        $this->form_validation->set_rules('duedate', 'due date', 'required');
    	$this->form_validation->set_rules('list', 'list', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

    	if ($this->form_validation->run() == FALSE)
    	{
    		$this->session->set_flashdata('error', validation_errors());
    		redirect('modullist/tambah');
		}
        else
        {
            $this->load->view('modullist/view');
        }

    	$this->load->model('modullist_model');


        $data = array(
        'kerjamodullist_kerjamodulid' => $this->input->post('modul'),
        'kerjamodullist_judul' => $this->input->post('list'),
        'kerjamodullist_kerjamodullistjenisid' => $this->input->post('listjenis'),
        'kerjamodullist_keterangan' => $this->input->post('keterangan'),
        'kerjamodullist_createby' => profile()['manajemenkerja_user_id'],
        'kerjamodullist_delete' => '0',
        'kerjamodullist_statusid' => '1'
         );
        $res = $this->modul_model->input_data('manajemenkerja_kerja_modul_list', $data);

        $duedate = DateTime::createFromFormat('m/d/Y', $this->input->post('duedate'));
        $dataduedate = array(
            'kerjamodullistduedate_kerjamodullistid' => $res,
            'kerjamodullistduedate_duedate' => $duedate->format('Y-m-d H:i:s'),
            'kerjamodullistduedate_createby' => profile()['manajemenkerja_user_id'],
            'kerjamodullistduedate_delete' => '0',
            'kerjamodullistduedate_aktif' => '1'
            );
        $resduedate = $this->modul_model->input_data('manajemenkerja_kerja_modul_list_duedate', $dataduedate);

    	redirect(site_url("modullist"),'refresh');
    }

    //menampilkan data yang terpilih
    public function update_data($id)
    {
        $data['status']         = $this->modullist_model->status_modul($id);
        $data['user']           = $this->modullist_model->data_user_all();
        $data['tim']            = $this->modullist_model->data_tim();
        $data['list']           = $this->modullist_model->data_modul_list($id);
        $data['timeline']       = $this->modullist_model->data_timeline($id);
        $data['listjenis']  = $this->modullist_model->getalldata('manajemenkerja_kerja_modul_list_jenis', 'kerjamodullistjenis_delete');

        $tim = array();
        
        foreach ($data['list']['list_member'] as $rows)
        {
            array_push($tim, $rows['kerjamodullistanggota_manajemenkerjauserid']);
        }

        $data['list']['modul_member_id'] = $tim;
        // trace($tim);
        $this->load->view('modullist/edit_view', $data);
    }

    public function detail_data($id)
    {
        // $data['pencapaian'] = $this->modul_model->pencapaian($id);
        // $data['status_modul'] = $this->modul_model->status_modul();
        $data['modullist']      = $this->modullist_model->getById($id);
        $data['judul']      = 'Modul List';
        // $data['user']       = $this->modul_model->data_user_all();
        // $data['tim']        = $this->modul_model->data_tim();
        // $data['list']       = $this->modul_model->data_modul_list($id);
        // $data['timeline']   = $this->modul_model->data_timeline($id);

        // $tim = array();
        
        // foreach ($data['modul']['kerja_member'] as $rows)
        // {
        //     array_push($tim, $rows['kerjamodulanggota_manajemenkerja_user_id']);
        // }

        // $data['modul']['modul_member_id'] = $tim;
        $this->load->view('modullist/detail_view', $data);
    }


    public function update_simpan_list()
    {
        foreach ($this->input->post('list') as $value)
        {
            $data = array(
            'kerjamodullist_status' => "1"
             );
            trace($data, FALSE);
            $this->modul_model->update_list_model($value,$data);
        }
        die();
		redirect(site_url("modul"),'refresh');
    }

    public function update_status_modul_list()
    {
        $status = $this->input->post('status');
        $id = $this->input->post('id');
        $modulid = $this->input->post('modulid');
        $statusnama = $this->input->post('statusnama');

        if ($status=='5')
        {
            $data = array(
            'kerjamodullist_statusid' => $status,
            'kerjamodullist_startdate' => date("Y-m-d H:i:s")
            );
            $res = $this->modullist_model->update_modul_status_model($id,$data, 'manajemenkerja_kerja_modul_list', 'kerjamodullist_id');
        }
        else if($status=='8')
        {
            $data = array(
            'kerjamodullist_statusid' => $status,
            'kerjamodullist_finish' => date("Y-m-d H:i:s")
            );
            $res = $this->modullist_model->update_modul_status_model($id,$data, 'manajemenkerja_kerja_modul_list', 'kerjamodullist_id');
        }
        else
        {
            $data = array(
            'kerjamodullist_statusid' => $status
            );
            // trace($data);
            $res = $this->modullist_model->update_modul_status_model($id,$data, 'manajemenkerja_kerja_modul_list', 'kerjamodullist_id');
            //menghitung persentase penyelesaian modul
        }
        $persentase = $this->modullist_model->data_modul_persentase($modulid);

        //update persen modul
        $datapersen = array (
                'kerjamodul_persen' => ($persentase['persen'])
            );
        $respersen = $this->modullist_model->update_persen_model($modulid,$datapersen);

        $data_timeline = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => 'Mengubah',
            'timeline_keterangan' =>  'mengubah status menjadi '.$statusnama,
            'timeline_modullistid' => $id,
            'timeline_simbol' => 'fa-hourglass-half bg-green'
            );
        $res_timeline = $this->modullist_model->timeline_model('manajemenkerja_timeline',$data_timeline);

        if ($res && $res_timeline)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));

    }

    public function update_status_modul_list_duedate()
    {
        $duedate = DateTime::createFromFormat('m/d/Y', $this->input->post('duedate'));
        $id = $this->input->post('id');

        $data = array(
            'kerjamodullistduedate_aktif' => '2'
            );

        $res = $this->modullist_model->update_modul_status_model($id, $data, 'manajemenkerja_kerja_modul_list_duedate', 'kerjamodullistduedate_kerjamodullistid');

        //insert duedate baru
        $data_duedate = array(
            'kerjamodullistduedate_kerjamodullistid' => $id,
            'kerjamodullistduedate_duedate' => $duedate->format('Y-m-d H:i:s'),
            'kerjamodullistduedate_createby' => profile()['manajemenkerja_user_id'],
            'kerjamodullistduedate_delete' => '0',
            'kerjamodullistduedate_aktif' => '1'
            );
        $simpan_duedate = $this->modullist_model->input_data('manajemenkerja_kerja_modul_list_duedate', $data_duedate);

        $data_timeline = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => 'Mengubah',
            'timeline_keterangan' =>  'mengubah duedate menjadi '.$duedate->format('d-m-Y H:i:s'),
            'timeline_modullistid' => $id,
            'timeline_simbol' => 'fa-calendar bg-red'
            );
        $res_timeline = $this->modullist_model->timeline_model('manajemenkerja_timeline',$data_timeline);

        if ($res && $res_timeline )
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));

    }
    public function update_status_modul_list_jenis()
    {
        $id = $this->input->post('id');
        $listjenis = $this->input->post('listjenis');
        $listjenis_nama = $this->input->post('nama');

        $data = array(
            'kerjamodullist_kerjamodullistjenisid' => $listjenis
            );
        $res = $this->modullist_model->update_modul_status_model($id, $data, 'manajemenkerja_kerja_modul_list', 'kerjamodullist_id');

        $data_timeline = array(
            'timeline_simbol' => $listjenis == '1' ? 'fa-plus-square bg-green': ($listjenis == "2" ? 'fa-bug bg-red' : 'fa-suitcase bg-blue'),
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => 'Mengubah',
            'timeline_keterangan' =>  'mengubah jenis list menjadi '.$listjenis_nama,
            'timeline_modullistid' => $id
            );
        $res_timeline = $this->modullist_model->timeline_model('manajemenkerja_timeline',$data_timeline);

        if ($res && $res_timeline)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));

    }

    public function update_status_modul_list_judul()
    {
        // $duedate = DateTime::createFromFormat('m/d/Y', $this->input->post('duedate'));
        $id = $this->input->post('id');
        $listjudul = $this->input->post('listjudul');

        $data = array(
            'kerjamodullist_judul' => $listjudul
            );
        // trace($data);
        $res = $this->modullist_model->update_modul_status_model($id, $data, 'manajemenkerja_kerja_modul_list', 'kerjamodullist_id');

        $data_timeline = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => 'Mengubah',
            'timeline_keterangan' =>  'mengubah nama list menjadi '.$listjudul,
            'timeline_modullistid' => $id,
            'timeline_simbol' => ' fa-file-text bg-blue'
            );
        // trace($data_timeline);
        $res_timeline = $this->modullist_model->timeline_model('manajemenkerja_timeline',$data_timeline);

        if ($res && $res_timeline)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));

    }

    public function update_status_modul_list_programmer()
    {
        $idprogrammer = $this->input->post('idprogrammer');
        $nama = $this->input->post('nama');
        $id = $this->input->post('id');
        // trace($id);

        $data = array(
            'kerjamodul_penanggungjawab' => $idprogrammer
            );
        // trace($idprogrammer);
        $res = $this->modullist_model->update_modul_status_model($id,$data, 'manajemenkerja_kerja_modul', 'kerjamodul_id');

        $data_timeline = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => 'Mengubah',
            'timeline_keterangan' =>  'mengubah programmer menjadi '.$nama,
            'timeline_modullistid' => $id,
            'timeline_simbol' => 'fa-exchange'
            );
        trace($data_timeline);
        $res_timeline = $this->modullist_model->timeline_model('manajemenkerja_timeline',$data_timeline);

        if ($res && $res_timeline)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));

    }

    public function komentar_insert()
    {
        $id = $this->input->post('id');

        $result_list = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul_list where kerjamodullist_id = "'.$id.'" ');

        $data = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => 'berkomentar',
            'timeline_keterangan' => $this->input->post('komentar'),
            'timeline_modullistid' => $id,
            'timeline_simbol' => 'fa fa-comments bg-blue'
            );
        
        $res = $this->modullist_model->timeline_model('manajemenkerja_timeline',$data);
        if ($res)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));
    }

    public function updateListById()
    {
        $id = $this->input->post('id');
        if ($this->input->post('status') == "tambah")
        {
            $valueStatus = '1';
        }
        else
        {
            $valueStatus = '0';
        }
        $data = array(
            'kerjamodullist_status' => $valueStatus
            );
        $res = $this->modullist_model->update_list_model($id,$data);

        $result_list = $this->db->query(' SELECT * FROM manajemenkerja_kerja_modul_list where kerjamodullist_id = "'.$id.'" ');
        $modulId = $result_list->row()->kerjamodullist_kerjamodulid;
        $keterangan = $result_list->row()->kerjamodullist_judul;

        if ($valueStatus == '1')
        {
            $judul ="Centang";
            $simbol = "fa-check";
        }
        else
        {
            $judul ="Urungkan";
            $simbol = "fa-square-o";
        }
            $data_timeline = array(
            'timeline_manajemenkerja_user_id' => profile()['manajemenkerja_user_id'],
            'timeline_judul' => $judul,
            'timeline_keterangan' =>  $keterangan,
            'timeline_modul' => $modulId,
            'timeline_simbol' => $simbol
            );
            $res = $this->modul_model->timeline_model('manajemenkerja_timeline',$data_timeline);
        
        if ($res)
        {
            $respons = array('status' => 'success');
            
        }else
        {
            $respons = array('status' => 'error');
        }
        die(json_encode($respons));

    }

    public function update_simpan($id)
    {
        // $startdate = DateTime::createFromFormat('m/d/Y', $this->input->post('startdate'));
        // $duedate = DateTime::createFromFormat('m/d/Y', $this->input->post('duedate'));
        // $owner = $this->input->post('owner');
        $anggota = $this->input->post('anggota[]');

        $data = array(
        'kerjamodullist_judul' => $this->input->post('judul'),
        'kerjamodul_penanggungjawab' => $this->input->post('penanggungjawab')
         );
        $this->modullist_model->update_model($id,$data);

        $dataanggota = array(
            'kerjamodulanggota_modulid' => $id
            );
        $this->modul_model->hard_delete_model($id, $dataanggota, 'manajemenkerja_kerja_modul_anggota', 'kerjamodulanggota_modulid');

        for ($i=0; $i < count($this->input->post('anggota')) ; $i++)
        { 
            $dataanggota = array(
                'kerjamodulanggota_modulid' => $id,
                'kerjamodulanggota_manajemenkerja_user_id' => $this->input->post('anggota')[$i],
                'kerjamodulanggota_delete' => '0'
                );
            $resanggota = $this->modul_model->input_data('manajemenkerja_kerja_modul_anggota', $dataanggota);
        }

        // //hapus list
        $datalist = array(
            'kerjamodullist_kerjamodulid' => $id
            );
        $this->modul_model->hard_delete_model($id, $datalist, 'manajemenkerja_kerja_modul_list', 'kerjamodullist_kerjamodulid');

        //insert list baru
        for ($i=0; $i < count($this->input->post('list')) ; $i++)
        { 
            $datalist = array(
                'kerjamodullist_kerjamodulid' => $id,
                'kerjamodullist_judul'  => $this->input->post('list')[$i],
                'kerjamodullist_delete' => '0',
                'kerjamodullist_status' => $this->input->post('status')[$i]
                );
            $reslist = $this->modul_model->input_data('manajemenkerja_kerja_modul_list', $datalist);
            // trace($datalist, FALSE);
        }
        // die();
        redirect(site_url("modul"),'refresh');
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $data = array (
                'kerjamodullist_delete' => '1'
            );

        $res = $this->modullist_model->update_model($id,$data);
        $data = array(
            'status' => 1,
            'message' => 'Berhasil hapus data karyawan'
        );
        die(json_encode($data));
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */