<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('validasi_login');
	}

	function index()
	{
		validasi_login();
		$this->load->view('dashboard_view');
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */