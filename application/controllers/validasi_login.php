<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ControllerName extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != 'login')
		{
			redirect('login');
		}
	}

	public function index()
	{
		
	}

}

/* End of file validasi_login.php */
/* Location: ./application/controllers/validasi_login.php */