<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
	}

	public function index()
	{
		if($this->session->userdata('login') == true)
		{
			redirect ('absen');
		}
		else
		{
			$this->load->view('login/login_view');
		}
	}

	function aksi_login()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$where = array(
			'manajemenkerja_user_username' => $username,
			'manajemenkerja_user_pw' => $password
		);

		$cek = $this->Login_model->cek_login("manajemenkerja_user",$where)->result_array();

		if(count($cek) > 0)
		{
			$data_session = array(
				'id' => $cek[0]['manajemenkerja_user_id'], 
				'nama' => $username,
				'status' => "login"
				);

			$this->session->set_userdata($data_session);
			
			redirect(('dashboard'));

		}
		else
		{
			 $this->session->set_flashdata('message', 'username atau password salah!');
			redirect(('login'));
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}