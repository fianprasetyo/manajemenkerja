<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('validasi_login');
		$this->load->model('user_model');
	}

	public function index()
	{
		$this->load->model('status_model');
		$data = $this->user_model->tampil_data('manajemenkerja_status');
		$data = array('data' => $data);
		$this->load->view('user/user_view', $data);
	}

	public function user_tambah()
	{
    	$this->load->view('user/user_tambah_view');
	}


    public function user_insert()
    {
    	$this->form_validation->set_rules('nama', 'nama', 'required');
    	$this->form_validation->set_rules('username', 'username', 'required');
    	$this->form_validation->set_rules('email', 'email', 'required');
    	$this->form_validation->set_rules('password', 'password', 'required');
    	$this->form_validation->set_rules('tanggallahir', 'tanggal lahir', 'required');
        $this->form_validation->set_rules('tanggalmasuk', 'tanggal masuk', 'required');
    	$this->form_validation->set_rules('foto', 'foto', 'required');

    	if ($this->form_validation->run() == FALSE)
    	{
    		$this->session->set_flashdata('error', validation_errors());
    		redirect('user/user_tambah');
		}
        else
        {
            $this->load->view('user/user_tambah_view');
        }

    	$this->load->model('user_model');

    	$tanggallahir = $this->input->post('tanggallahir');
    	$tanggallahir = explode("/", $tanggallahir);
    	$tanggallahirformat = $tanggallahir[2].'-'.$tanggallahir[1].'-'.$tanggallahir[0];

    	$tanggalmasuk = $this->input->post('tanggalmasuk');
    	$tanggalmasuk = explode("/", $tanggalmasuk);
    	$tanggalmasukformat = $tanggalmasuk[2].'-'.$tanggalmasuk[1].'-'.$tanggalmasuk[0];


    	$data = array(
        'manajemenkerja_user_nama' => $this->input->post('nama'),
        'manajemenkerja_user_username' => $this->input->post('username'),
        'manajemenkerja_user_email' => $this->input->post('email'),
        'manajemenkerja_user_pw' => md5($this->input->post('password')),
        'manajemenkerja_user_lahir' => $tanggallahirformat,
        'manajemenkerja_user_masuk' => $tanggalmasukformat,
        'manajemenkerja_user_createby' => profile()['manajemenkerja_user_id']
         );
    	// echo "<pre>";
    	// print_r($data);
    	// echo "</pre>";
    	// die();
    	$res = $this->user_model->input_data('manajemenkerja_user', $data);
    	redirect(site_url("user"),'refresh');
    }

    public function user_update_data($id)
    {
        $data['hasil'] = $this->user_model->getById($id);
        $this->load->view('user/user_edit_view', $data);
 
    }

    public function user_detail_data($id)
    {
        $data['hasil'] = $this->user_model->getById($id);
        $this->load->view('user/user_detail_view', $data);
 
    }
    

    public function user_report_data($id)
    {
        $data['hasil'] = $this->user_model->data_user($id);
        $data['list'] = $this->user_model->data_list($id);
        $data['listterlambat'] = $this->user_model->data_list_terlambat($id);
        $data['modul'] = $this->user_model->data_modul($id);
        $data['modulid'] = $this->user_model->data_modulid($id);
        // $data['selisih'] = $this->user_model->data_selisih();
        $jumlahmodulid = count($data['modulid']);


        for ($i=0; $i < count($jumlahmodulid) ; $i++)
        {
            $data['modullist_notin_done'] = $this->user_model->data_modullist_notin_done($id, $i);
           
        }

        $this->load->view('user/report', $data);
 
    }

    public function user_laporan($id, $awal, $akhir)
    {
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $data['hasil'] = $this->user_model->data_user($id);
        // $data['list'] = $this->user_model->data_list($id);
        $data['list'] = $this->user_model->data_list($id, $awal, $akhir);
        $data['listterlambat'] = $this->user_model->data_list_terlambat($id, $awal);
        $data['modul'] = $this->user_model->data_modul($id);
        $data['modulid'] = $this->user_model->data_modulid($id);
        // $data['selisih'] = $this->user_model->data_selisih();
        $jumlahmodulid = count($data['modulid']);

        for ($i=0; $i < count($jumlahmodulid) ; $i++)
        {
            $data['modullist_notin_done'] = $this->user_model->data_modullist_notin_done($id, $i);
           
        }
        $this->load->view('user/print_laporan', $data);
    }

    public function user_report_cari()
    {
        $id  = $this->input->post('iduser');
        $this->form_validation->set_rules('rentangwaktu', 'rentangwaktu', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error', validation_errors());
            redirect('user/user_report_data/'.$id);
        }
        else
        {
            $rentangwaktu  = $this->input->post('rentangwaktu');
            $ex_rentangwaktu = explode(' - ', $rentangwaktu);
            $awal_rentangwaktu = $ex_rentangwaktu['0'];
            $ex_rentangwaktu_format = DateTime::createFromFormat('m/d/Y', $awal_rentangwaktu);
            $ex_rentangwaktu_format_baru = $ex_rentangwaktu_format->format('Y-m-d');

            $akhir_rentangwaktu = $ex_rentangwaktu['1'];
            $akhir_rentangwaktu_format = DateTime::createFromFormat('m/d/Y', $akhir_rentangwaktu);
            $ex_akhir_rentangwaktu_format_baru = $akhir_rentangwaktu_format->format('Y-m-d');

            $data['awal'] = $ex_rentangwaktu_format_baru;
            $data['akhir'] = $ex_akhir_rentangwaktu_format_baru;
            $data['hasil'] = $this->user_model->data_user($id);
            $data['modul'] = $this->user_model->data_modul($id);
            $data['list'] = $this->user_model->data_list($id, $ex_rentangwaktu_format_baru, $ex_akhir_rentangwaktu_format_baru);
            $data['listterlambat'] = $this->user_model->data_list_terlambat($id, $ex_rentangwaktu_format_baru);
            // trace($data['listterlambat']);

            $this->load->view('user/report', $data);
            
        }

    }

    public function user_update_simpan()
    {
    	$id= $this->input->post('id');

    	$nama  = $this->input->post('nama');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $tanggallahir = $this->input->post('tanggallahir');
    	$tanggallahir = explode("/", $tanggallahir);
    	$tanggallahirformat = $tanggallahir[2].'-'.$tanggallahir[1].'-'.$tanggallahir[0];

    	$tanggalmasuk = $this->input->post('tanggalmasuk');
    	$tanggalmasuk = explode("/", $tanggalmasuk);
    	$tanggalmasukformat = $tanggalmasuk[2].'-'.$tanggalmasuk[1].'-'.$tanggalmasuk[0];

    	if (is_null($password))
    	{
    		$data = array (
	            'manajemenkerja_user_nama' => $nama,
	            'manajemenkerja_user_username'  => $username,
	            'manajemenkerja_user_email' => $email,
	            'manajemenkerja_user_lahir' => $tanggallahirformat,
	            'manajemenkerja_user_masuk' => $tanggalmasukformat
        	);
    	}
    	else
    	{
    		$data = array (
	            'manajemenkerja_user_nama' => $nama,
	            'manajemenkerja_user_username'  => $username,
	            'manajemenkerja_user_email' => $email,
	            'manajemenkerja_user_pw' => md5($password),
	            'manajemenkerja_user_lahir' => $tanggallahirformat,
	            'manajemenkerja_user_masuk' => $tanggalmasukformat
        	);
    	}

		$this->user_model->user_update_model($id,$data);
		redirect(site_url("user"),'refresh');
    }

    public function user_delete()
    {
        $id = $this->input->post('id');
    	$data = array (
	            'manajemenkerja_user_delete' => '1'
        	);

    	$res = $this->user_model->user_update_model($id,$data);

        $data = array(
            'status' => 1,
            'message' => 'Berhasil hapus data karyawan'
        );
        die(json_encode($data));
		redirect(site_url("user"),'refresh');
    }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */