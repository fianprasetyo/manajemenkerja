<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit List | manajemen Kerja</title>
  <?php $this->load->view('head'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <?php $this->load->view('profile_view'); ?>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
  <?php $this->load->view('menu_view');?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit List
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Advanced Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
            <?php if ( $this->session->flashdata('error')): ?>
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                <?php print $this->session->flashdata('error'); ?>
              </div>
            <?php endif ?>

          <a href="" id="btn-refresh"><button type="button" class="btn btn-success"><i class="fa fa-refresh"></i></button></a>
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header --><?php
        // trace($modul);
         ?>
       <form method="post" action="<?php print site_url("modullist/update_simpan/".$list['kerjamodullist_id']); ?>">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                  <div class="box-header">
                    <h3 class="box-title">Informasi List</h3>
                  </div>
                  <div class="box-body no-padding">
                    <table class="table table-condensed">
                    <input name="modullistid" type="hidden" value= "<?php print $list['kerjamodullist_id']; ?>" ></input>
                    <input name="modulid" type="hidden" value= "<?php print $list['kerjamodul_id']; ?>" ></input>
                      <tr>
                        <th>Kerja</th>
                        <td ><?php print $list['kerja_judul']; ?></td>
                      </tr>
                      <tr>
                        <th>Modul</th>
                        <td><?php print $list['kerjamodul_judul']; ?></td>
                      </tr>
                      <tr>
                        <th>Keterangan Modul</th>
                        <td><?php print $list['kerjamodul_keterangan']; ?></td>
                      </tr>
                      <tr>
                        <th>Jenis List</th>
                        <td>
                          <div id="div_listjenis" class="form-group">
                            <select id="listjenis" class="form-control select2" data-placeholder="Pilih Jenis List" style="width: 100%;" name="listjenis">
                                <?php
                                  foreach ($listjenis as $row)
                                  {
                                    if(($list['kerjamodullist_kerjamodullistjenisid']) == ($row['kerjamodullistjenis_id']))
                                    {
                                      ?>
                                      <option selected="selected" value="<?php print $row['kerjamodullistjenis_id'];?>"><?php print $row['kerjamodullistjenis_nama'];?></option>
                                    <?php }
                                  else
                                  {
                                    ?>
                                    <option value="<?php print $row['kerjamodullistjenis_id'];?>"><?php print $row['kerjamodullistjenis_nama'];?></option>
                                  <?php }
                                  
                                } ?>
                            </select>
                        </div>
                        </td>
                      </tr>
                      <tr>
                        <th>Status</th>
                        <td>
                          <div id="statusmodul" class="form-group">
                            <select id="statusmodul" class="form-control select2" data-placeholder="Pilih Status" style="width: 100%;" name="status" id="status">
                                <?php
                                  foreach ($status as $row)
                                  {
                                    if(($list['kerjamodullist_statusid']) == ($row['status_id']))
                                    {
                                      ?>
                                      <option selected="selected" value="<?php print $row['status_id'];?>"><?php print $row['status_nama'];?></option>
                                    <?php }
                                  else
                                  {
                                    ?>
                                    <option value="<?php print $row['status_id'];?>"><?php print $row['status_nama'];?></option>
                                  <?php }
                                  
                                } ?>
                            </select>
                        </div>
                        </td>
                      </tr>
                      <tr>
                        <th>Duedate</th>
                        <td>
                          <div id="duedatediv" class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                              </div>
                              <?php $date   = date('m/d/Y', strtotime($list['kerjamodullistduedate_duedate'])); ?>
                              <input type="text" class="form-control pull-right" id="duedate" placeholder="Due Date" name="duedate[]" value="<?php print $date ?>">
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th>List</th>
                        <td>
                          <div id="listjudul" class="input-group">
                            <input name="listjudul" placeholder="Judul" type="text" class="form-control" value="<?php print $list['kerjamodullist_judul']; ?>" >
                            <div class="input-group-btn">
                              <button id="simpanjudul" type="button" class="btn btn-info">Simpan</button>
                            </div>
                            <!-- /btn-group -->
                          </div>
                          <!-- <div class="form-group">
                            <textarea class="form-control" rows="3" placeholder="judul" name = "judul">
                              <?php print $list['kerjamodullist_judul']; ?>
                            </textarea>
                          </div> -->
                        </td>
                      </tr>
                      <tr>
                      <tr>
                        <th>Programmer</th>
                        <td>
                          <div id="penanggungjawab" class="form-group">
                            <select disabled class="form-control select2" data-placeholder="Pilih Penanggung Jawab" style="width: 100%;" name="penanggungjawab" id="penanggungjawab">
                              <?php
                                foreach ($user as $row)
                                {
                                  if(($list['kerjamodul_penanggungjawab']) == ($row['manajemenkerja_user_id']))
                                  {
                                    ?>
                                    <option selected="selected" value="<?php print $list['kerjamodul_penanggungjawab'];?>"><?php print $row['manajemenkerja_user_nama'];?></option>
                                  <?php }
                                else
                                {
                                  ?>
                                  <option value="<?php print $row['manajemenkerja_user_id'];?>"><?php print $row['manajemenkerja_user_nama'];?></option>
                                <?php }
                                
                              } ?>
                            </select>
                          </div>
                        </td>
                      </tr>
                        <th>Tester</th>
                        <td>
                          <div class="form-group">
                            <select disabled class="form-control select2" data-placeholder="Pilih Anggota" style="width: 100%;" name="anggota[]" id="anggota">
                              <?php
                                foreach ($user as $row)
                                {
                                  if(($list['kerjamodul_tester']) == ($row['manajemenkerja_user_id']))
                                  {
                                    ?>
                                    <option selected="selected" value="<?php print $list['kerjamodul_penanggungjawab'];?>"><?php print $row['manajemenkerja_user_nama'];?></option>
                                  <?php }
                                else
                                {
                                  ?>
                                  <option value="<?php print $row['manajemenkerja_user_id'];?>"><?php print $row['manajemenkerja_user_nama'];?></option>
                                <?php }
                                
                              } ?>
                            </select>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
              </div>
              <div class="col-md-6">
                <div class="input-group input-group-lg">
                </div>
                <div class="input-group">
                  <input name="komentar" placeholder="komentar" type="text" class="form-control">
                      <span class="input-group-btn">
                        <button id="simpankomentar" type="button" class="btn btn-info">Simpan</button>
                      </span>
                </div>
                </br>

                <!-- The time line -->
                <ul class="timeline">
                  <!-- timeline time label -->
                  <?php foreach ($timeline as $row) {?>
                  <li class="time-label">
                        <span class="bg-red">
                          <?php print $row['tanggal'] ?>
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa <?php print $row['timeline_simbol']?>"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> <?php print $row['waktu'] ?></span>

                      <h3 class="timeline-header"><a href="#"><?php print $row['manajemenkerja_user_nama'] ?></a> <?php print $row['timeline_judul'] ?></h3>

                      <div class="timeline-body">
                        <?php print $row['timeline_keterangan'] ?>
                      </div>
                      <div class="timeline-footer">
                      </div>
                    </div>
                  </li>
                  <?php } ?>
                </ul>
              </div>
              <!-- /.col -->

              <!-- /.col -->
            </div>
          <!-- /.row -->
        </div>
          <!-- /.box-footer -->
        </form>

        
      </div>
      <!-- /.box -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.3
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#duedate, #startdate').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });

  $(document).ready(function(){
    var next = 1;
    $("#b1").click(function(e){
        e.preventDefault();

        var lastValue = $("#add-row").find('input').eq(0).val();
        var lastStatus = $("#add-row").find('input').eq(1).val();
                    $("#add-row").find('input').eq(0).val("");
                    $("#add-row").find('input').eq(1).val(0);
        var form = "<div class='form-group'>"+
                  "<input name = 'list[]' placeholder='List' style=' width: 92%; display: inline; float: left;' autocomplete='off' class='form-control' id='field' name='list[]' type='text' value='"+lastValue+"'>"+
                  "<input name = 'status[]' type='hidden' value='"+lastStatus+"'></input>"+
                  "<button id='remove' class='btn btn-danger remove-me' ><i class='fa fa-trash-o'></i></button>"+
                    "</div>";
        $(form).insertBefore( "#add-row" );
    });
    
    $("#task-list").on('click', '.remove-me' ,function(e){
      e.preventDefault();
      console.log("Test");
      $(this).parent('.form-group').remove();
    });

    $("#simpankomentar").on('click', function()
      {
      var komentar = $(this).parents('.input-group').find('input').val();
      var modullistid = $("input[name='modullistid']").val();
      console.log(komentar);
      swal
      (
        {
          title: "Are you sure?",
          text: "",
          type: "info",
          showCancelButton: true,
          // confirmButtonColor: "blue",
          confirmButtonText: "Yes!",
          // cancelButtonText: "No!",
          // closeOnConfirm: false,
          showLoaderOnConfirm: true
          // closeOnCancel: false
        },
        function(isConfirm)
          {
            
            if (isConfirm)
            {
              var url = "<?php echo site_url('modullist/komentar_insert/'); ?>";
              $.ajax
                  ({
                    type: "POST",
                    url: url,
                    data: {id : modullistid, komentar : komentar },
                    success: function(res)
                      {
                        $("#btn-refresh")[0].click();
                        console.log(res);
                      },
                      dataType: "JSON"
                  });
            }
            else
            {
              swal("Cancelled", "Your imaginary file is safe :)", "error");
              $("#btn-refresh")[0].click();
            }
          }
      );
      
    })

    $("#simpanjudul").on('click', function()
    {
    var listjudul = $(this).parents('.input-group').find('input').val();
    var modullistid = $("input[name='modullistid']").val();
    console.log(listjudul);
    swal
      (
        {
          title: "Are you sure?",
          text: "",
          type: "info",
          showCancelButton: true,
          // confirmButtonColor: "blue",
          confirmButtonText: "Yes!",
          // cancelButtonText: "No!",
          // closeOnConfirm: false,
          showLoaderOnConfirm: true
          // closeOnCancel: false
        },
        function(isConfirm)
        {
          if (isConfirm)
          {
            var url = "<?php echo site_url('modullist/update_status_modul_list_judul/'); ?>";
              $.ajax
                  ({
                    type: "POST",
                    url: url,
                    data: {id : modullistid, listjudul : listjudul },
                    success: function(res)
                      {
                        $("#btn-refresh")[0].click();
                        console.log(res);
                      },
                      dataType: "JSON"
                  });
          }
          else
          {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
            $("#btn-refresh")[0].click();
          }
        }
      );
    })

    $("#statusmodul").on('change',"#statusmodul", function(){
    var status = $(this).val();
    var statusnama = $("#statusmodul option:selected").text();
    var modullistid = $("input[name='modullistid']").val();
    var modulid = $("input[name='modulid']").val();
    swal
      (
        {
          title: "Are you sure?",
          text: "",
          type: "info",
          showCancelButton: true,
          // confirmButtonColor: "blue",
          confirmButtonText: "Yes!",
          // cancelButtonText: "No!",
          // closeOnConfirm: false,
          showLoaderOnConfirm: true
          // closeOnCancel: false
        },
        function(isConfirm)
          {
            
            if (isConfirm)
            {
              var url = "<?php echo site_url('modullist/update_status_modul_list/'); ?>";
              $.ajax
                  ({
                    type: "POST",
                    url: url,
                    data: {statusnama : statusnama, status : status, id : modullistid, modulid : modulid },
                    success: function(res)
                      {
                        $("#btn-refresh")[0].click();
                        console.log(res);
                      },
                      dataType: "JSON"
                  });
            }
            else
            {
              swal("Cancelled", "Your imaginary file is safe :)", "error");
              $("#btn-refresh")[0].click();
            }
          }
      );
    });

    $("#penanggungjawab").on('change',"#penanggungjawab", function(){
      var idprogrammer = $(this).val();
      var namaprogrammer = $("#penanggungjawab option:selected").text();
      var modulid = $("input[name='modulid']").val();
      swal
        (
          {
            title: "Are you sure?",
            text: "",
            type: "info",
            showCancelButton: true,
            // confirmButtonColor: "blue",
            confirmButtonText: "Yes!",
            // cancelButtonText: "No!",
            // closeOnConfirm: false,
            showLoaderOnConfirm: true
            // closeOnCancel: false
          },
          function(isConfirm)
            {
              
              if (isConfirm)
              {
                var url = "<?php echo site_url('modullist/update_status_modul_list_programmer/'); ?>";
                $.ajax
                    ({
                      type: "POST",
                      url: url,
                      data: {idprogrammer : idprogrammer, nama : namaprogrammer, id : modulid },
                      success: function(res)
                        {
                          $("#btn-refresh")[0].click();
                          console.log(res);
                        },
                        dataType: "JSON"
                    });
              }
              else
              {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                $("#btn-refresh")[0].click();
              }
            }
        );
    });
    
    $("#duedatediv").on('change',"#duedate", function(){
      var duedate = $(this).val();
      var modullistid = $("input[name='modullistid']").val();
      swal
        (
          {
            title: "Are you sure?",
            text: "",
            type: "info",
            showCancelButton: true,
            // confirmButtonColor: "blue",
            confirmButtonText: "Yes!",
            // cancelButtonText: "No!",
            // closeOnConfirm: false,
            showLoaderOnConfirm: true
            // closeOnCancel: false
          },
          function(isConfirm)
            {
              
              if (isConfirm)
              {
                var url = "<?php echo site_url('modullist/update_status_modul_list_duedate/'); ?>";
                $.ajax
                    ({
                      type: "POST",
                      url: url,
                      data: {duedate : duedate, id : modullistid },
                      success: function(res)
                        {
                          $("#btn-refresh")[0].click();
                          console.log(res);
                        },
                        dataType: "JSON"
                    });
              }
              else
              {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                $("#btn-refresh")[0].click();
              }
            }
        );
    });

    $("#div_listjenis").on('change',"#listjenis", function(){
      var listjenis = $(this).val();
      var listjenis_nama = $("#listjenis option:selected").text();
      var modullistid = $("input[name='modullistid']").val();
      swal
        (
          {
            title: "Are you sure?",
            text: "",
            type: "info",
            showCancelButton: true,
            // confirmButtonColor: "blue",
            confirmButtonText: "Yes!",
            // cancelButtonText: "No!",
            // closeOnConfirm: false,
            showLoaderOnConfirm: true
            // closeOnCancel: false
          },
          function(isConfirm)
            {
              
              if (isConfirm)
              {
                var url = "<?php echo site_url('modullist/update_status_modul_list_jenis/'); ?>";
                $.ajax
                    ({
                      type: "POST",
                      url: url,
                      data: {nama : listjenis_nama, listjenis : listjenis, id : modullistid },
                      success: function(res)
                        {
                          $("#btn-refresh")[0].click();
                          console.log(res);
                        },
                        dataType: "JSON"
                    });
              }
              else
              {
                swal("Cancelled", "Your imaginary file is safe :)", "error");
                $("#btn-refresh")[0].click();
              }
            }
        );
    });

});
</script>
</body>
</html>
