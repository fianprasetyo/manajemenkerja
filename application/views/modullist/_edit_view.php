<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit List | manajemen Kerja</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datepicker/datepicker3.css'); ?>">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/all.css'); ?>">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/colorpicker/bootstrap-colorpicker.min.css'); ?>">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css'); ?>">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/select2.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <?php $this->load->view('profile_view'); ?>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
  <?php $this->load->view('menu_view');?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit List
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Advanced Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <a href="" id="btn-refresh"><button type="button" class="btn btn-success"><i class="fa fa-refresh"></i></button></a>
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header --><?php
        // trace($modul);
         ?>
       <form method="post" action="<?php print site_url("modullist/update_simpan/".$list['kerjamodullist_id']); ?>">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                  <div class="box-header">
                    <h3 class="box-title">Informasi List</h3>
                  </div>
                  <div class="box-body no-padding">
                    <table class="table table-condensed">
                      <tr>
                        <th>Kerja</th>
                        <td ><?php print $list['kerja_judul']; ?></td>
                      </tr>
                      <tr>
                        <th>Modul</th>
                        <td><?php print $list['kerjamodul_judul']; ?></td>
                      </tr>
                      <tr>
                        <th>Keterangan Modul</th>
                        <td><?php print $list['kerjamodul_keterangan']; ?></td>
                      </tr>
                      <tr>
                        <th>List</th>
                        <td>
                          <div class="form-group">
                            <textarea class="form-control" rows="3" placeholder="list" name = "list">
                              <?php print $list['kerjamodullist_judul']; ?>
                            </textarea>
                          </div>
                        </td>
                      </tr>
                      <tr>
                      <tr>
                        <th>Status List</th>
                        <td>
                          <select id="statusmodul" class="form-control select2" data-placeholder="Pilih Status" style="width: 100%;" name="status" id="status">
                            <?php
                              foreach ($status as $row)
                              {
                                if(($modul['kerjamodullis_statusid']) == ($row['status_id']))
                                {
                                  ?>
                                  <option selected="selected" value="<?php print $row['status_id'];?>"><?php print $row['status_nama'];?></option>
                                <?php }
                              else
                              {
                                ?>
                                <option value="<?php print $row['status_id'];?>"><?php print $row['status_nama'];?></option>
                              <?php }
                              
                            } ?>
                          </select>
                        </div>
                        </td>
                      </tr>
                        <th>Start Date</th>
                          <?php
                            $datecreatedate = $list['kerjamodullist_createdate'];
                            $datecreatedate   = date('m-d-Y', strtotime($datecreatedate));
                          ?>
                        <td>
                          <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="duedate" placeholder="Due Date" name="duedate" value= "<?php print $datecreatedate; ?>" >
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th>Due Date</th>
                        <?php
                          $dateduedate = $list['kerjamodullist_duedate'];
                          $dateduedate   = date('m-d-Y', strtotime($dateduedate));
                        ?>

                        <td>
                          <div class="form-group">
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="duedate" placeholder="Due Date" name="duedate" value= "<?php print $dateduedate ?>" >
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th>Penanggung Jawab</th>
                        <td>
                          <div class="form-group">
                            <select class="form-control select2" data-placeholder="Pilih Penanggung Jawab" style="width: 100%;" name="penanggungjawab" id="penanggungjawab">
                              <?php
                                foreach ($user as $row)
                                {
                                  if(($list['kerjamodul_penanggungjawab']) == ($row['manajemenkerja_user_id']))
                                  {
                                    ?>
                                    <option selected="selected" value="<?php print $list['kerjamodul_penanggungjawab'];?>"><?php print $row['manajemenkerja_user_nama'];?></option>
                                  <?php }
                                else
                                {
                                  ?>
                                  <option value="<?php print $row['manajemenkerja_user_id'];?>"><?php print $row['manajemenkerja_user_nama'];?></option>
                                <?php }
                                
                              } ?>
                            </select>
                          </div>
                        </td>
                      </tr>
                        <th>Anggota</th>
                        <td>
                          <div class="form-group">
                            <select class="form-control select2" multiple="multiple" data-placeholder="Pilih Anggota" style="width: 100%;" name="anggota[]" id="anggota">
                              <?php foreach ($user as $row): 

                              $strSelected = in_array($row['manajemenkerja_user_id'], $list['modul_member_id']) ? "selected='selected'" : "";

                              ?>
                                <option value="<?php echo $row['manajemenkerja_user_id']; ?>" <?php echo $strSelected; ?>>
                                  <?php echo $row['manajemenkerja_user_nama']; ?>
                                </option>
                              <?php endforeach ?>
                            </select>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
              </div>
              <!-- /.col -->

              <!-- /.col -->
            </div>
          <!-- /.row -->
        </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <button type="cancel" class="btn btn-default">Cancel</button>
            <button type="submit" class="btn btn-info pull-right">Tambah</button>
          </div>
          <!-- /.box-footer -->
        </form>

        
      </div>
      <!-- /.box -->


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.3
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url('assets/plugins/select2/select2.full.min.js'); ?>"></script>
<!-- InputMask -->
<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js'); ?>"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo base_url('assets/plugins/daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url('assets/plugins/colorpicker/bootstrap-colorpicker.min.js'); ?>"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.js'); ?>"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#duedate, #startdate').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });

  $(document).ready(function(){
    var next = 1;
    $("#b1").click(function(e){
        e.preventDefault();

        var lastValue = $("#add-row").find('input').eq(0).val();
        var lastStatus = $("#add-row").find('input').eq(1).val();
                    $("#add-row").find('input').eq(0).val("");
                    $("#add-row").find('input').eq(1).val(0);
        var form = "<div class='form-group'>"+
                  "<input name = 'list[]' placeholder='List' style=' width: 92%; display: inline; float: left;' autocomplete='off' class='form-control' id='field' name='list[]' type='text' value='"+lastValue+"'>"+
                  "<input name = 'status[]' type='hidden' value='"+lastStatus+"'></input>"+
                  "<button id='remove' class='btn btn-danger remove-me' ><i class='fa fa-trash-o'></i></button>"+
                    "</div>";
        $(form).insertBefore( "#add-row" );
    });
    
    $("#task-list").on('click', '.remove-me' ,function(e){
      e.preventDefault();
      console.log("Test");
      $(this).parent('.form-group').remove();
    });
    
});
</script>
</body>
</html>
