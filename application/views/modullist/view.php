
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Data modul List | Manajemen Kerja </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css'); ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.min.css'); ?>">

  <link rel="stylesheet" type="text/css" href="<?php print base_url('assets/sweetalert-master/dist/sweetalert.css'); ?>">
  <script type="text/javascript" src="<?php print base_url('assets/sweetalert-master/dist/sweetalert.min.js'); ?>"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b></b>Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <?php $this->load->view('profile_view'); ?>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <?php $this->load->view('menu_view');?>

    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data List
        <small><!-- it all starts here --></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

          <div class="box-header with-border">

          <a href="<?php print site_url('modullist');?>" id="btn-refresh"><button type="button" class="btn btn-success"><i class="fa fa-refresh"></i></button></a>
          <a href="<?php print site_url('modullist/tambah') ?>"><button type="button" class="btn btn-info"><i class="fa fa-plus"></i></button></a>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>

            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="width:10px">No</th>
                  <th style="width:100px">modul</th>
                  <th style="width:200px">List</th>
                  <th style="width:100px">Due date</th>
                  <th style="width:100px">Penanggung Jawab</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>

                <?php
                  $no = 1;
                  foreach ($data as $buda)
                  {
                    // $startdate = DateTime::createFromFormat('Y-m-d H:i:s', $buda['kerjamodul_startdate']);
                    $duedate = DateTime::createFromFormat('Y-m-d H:i:s', $buda['kerjamodullistduedate_duedate']);
                    // trace($duedate);
                    ?>
                <tr>
                  <td ><?php print $no++; ?></td>
                  <td><?php print $buda['kerjamodul_judul']; ?></td>
                  <td><?php print $buda['kerjamodullist_judul']; ?></td>
                  <td><?php print $duedate->format('d-m-Y'); ?></td>
                  <td><?php print $buda['manajemenkerja_user_nama']; ?></td>
                  <td><?php print $buda['status_nama']; ?></td>
                  <td>

                    <div class="btn-group">
                      <a href="<?php echo site_url ('modullist/update_data/'.$buda['kerjamodullist_id']); ?>">
                        <button type="button" class="btn btn-warning"><i class="fa fa-edit"></i>
                        </button>
                      </a>
                    </div>
                        <button onclick="return confirmhapus('<?php echo $buda['kerjamodullist_id']; ?>')" type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i>
                        </button>
                    </div>
                    <div class="btn-group">
                      <a href="<?php echo site_url ('modullist/detail_data/'.$buda['kerjamodullist_id']); ?>">
                        <button type="button" class="btn btn-default"><i class="fa fa-desktop"></i></button>
                        </a>
                    </div>
                  </td>
                </tr>

                <?php } ?>
                  
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>modul</th>
                  <th>List</th>
                  <th>Due date</th>
                  <th>Penanggung Jawab</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2017 <a href="http://fianprasetyo.com">Fian Prasetyo</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url('assets/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/plugins/fastclick/fastclick.js'); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/dist/js/app.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/dist/js/demo.js'); ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });

  function confirmhapus(id)
    {
      swal
      (
        {
          title: "Are you sure?",
          text: "You will not be able to recover this imaginary file!",
          type: "info",
          showCancelButton: true,
          // confirmButtonColor: "blue",
          confirmButtonText: "Yes!",
          // cancelButtonText: "No!",
          // closeOnConfirm: false,
          showLoaderOnConfirm: true
          // closeOnCancel: false
        },

        function(isConfirm)
          {
            
            if (isConfirm)
            {

              var url = "<?php echo site_url('modullist/delete/'); ?>";
              $.ajax
              ({
                type: "POST",
                url: url,
                data: {id : id},
                success: function(res)
                  {
                  
                  if(res.status == 1)
                    {
                    swal("Deleted!", res.message , "success");   
                    setTimeout(function()
                      {
                        $("#btn-refresh")[0].click();
                      }, 2000);
                    }
                    console.log(res);
                  },
                  dataType: "JSON"
              });
          }
          else
          {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
          }
          
        }
      );
    }

</script>
</body>
</html>
