<section class="sidebar">
<!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li>
      <a href="<?php print base_url('index.php/dashboard'); ?>">
      <i class="fa fa-dashboard"></i>
      <span>Dashboard</span>
      <span class="label label-primary pull-right"></span>
      </a>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-reorder"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php print base_url('index.php/user') ?> "><i class="fa fa-users"></i>User</a></li>
        <li><a href="<?php print base_url('index.php/user') ?> "><i class="fa fa-users"></i>User</a></li>
        <li><a href="<?php print base_url('index.php/status') ?> "><i class="fa fa-hourglass-start"></i>Status</a></li>
        <li><a href="<?php print base_url('index.php/posisi') ?> "><i class="fa fa-user"></i>Posisi</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-file-text"></i> <span>Manajemen Kerja</span> <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php print base_url('index.php/kerja') ?> "><i class="fa fa-suitcase"></i>Kerja</a></li>
        <li><a href="<?php print base_url('index.php/modul') ?> "><i class="fa fa-files-o"></i>Modul</a>
        <li><a href="<?php print base_url('index.php/Modullist') ?> "><i class="fa fa-file-o"></i>List</a>
        </li>
      </ul>
    </li>

  </ul>
</section>
