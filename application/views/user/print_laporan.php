<?php
  ob_start();
?>
 
<style>
td {
    padding: 3px 5px 3px 5px;
    border-right: 1px solid #666666;
    border-bottom: 1px solid #666666;
}
 
.head td {
    font-weight: bold;
    font-size: 12px;
}
 
table .main tbody tr td {
    font-size: 12px;
}
 
table, table .main {
    width: 100%;
    border-top: 1px solid #666;
    border-left: 1px solid #666;
    border-collapse: collapse;
    background: #fff;
}
.profil
{
  border:2px solid lightgray;
  width: 200px;
  padding: 10px;
  float: center;
  text-align: center;
  float: left;
}

.bigtarget
{
  border:2px solid lightgray;
  width: 520px;
  float: center;
  text-align: center;
  float: left;
  margin-left: 9px;
  
}
.bigtargetdetail
{
  width: 200px;
  float: left;
  padding: 20px 20px 7px 21px;

}

.judul
{
  padding: 10px;
  font-size: 20px;
  color: white;
  background: red;
}

ul
{
  list-style: none;
}
 
h1 {
    font-size:20px;
}
.persentase
{
  background-color: red;
  color: white;
  height: 115px;
  width: 115px;
  text-align: center;
  font-size: 30px;
  line-height: 115px;
  border-radius:115px;
}

.motto
{
  background-color: red;
  color: white;
  border-radius: 15px;
  padding: 5px;
}

.selisih
{
  background-color: green;
  color: white;
  height: 1px;
  width: 1px;
  text-align: center;
  line-height: 1px;
  border-radius:1px;
}

.judul_bigtarget, .box-title
{
  border-bottom: 1px solid lightgray;
  text-align: left;
  padding-top: 10px;
  padding-left: 10px;
  padding-bottom: 10px;
}
.box-title
{
  border: 1px solid lightgray;
}
.bigtarget_label
{
  margin: 10px;
  color: red;
}

.box-title
{
  color: red;
}
</style>
  <div class="profil">
      <img src="<?php print base_url('assets/dist/img/user4-128x128.jpg') ?>">

    <h3 class="profile-username text-center"><?php print $hasil['manajemenkerja_user_nama']; ?></h3>

    <p class="text-muted text-center"><?php print $hasil['posisi_nama'] ?></p>
    </br>
    <div class="motto">
      <label><?php print $hasil['manajemenkerja_user_motto'] ?></label>
    </div>
  </div>
  <div class="bigtarget">
    <div class="judul_bigtarget">
      <b class="bigtarget_label">BIG TARGET</b>
    </div>
<div style="position: absolute; left: 0; right: 0; margin-left: auto;margin-right: auto;width: 487px;" >
    <?php  
      foreach ($modul as $row) {?>
        
          <div class="bigtargetdetail">
            <div style="width: 120px; margin: auto; text-align: center;">
              <div class="persentase">
                  <label><?php print $row['kerjamodul_persen']."%"; ?></label>
              </div>
            </div>
            <h3 class="profile-username text-center"><?php print $row['kerjamodul_judul'] ?></h3>
            <?php $duedatemodul = DateTime::createFromFormat('Y-m-d H:i:s', $row['kerjamodul_duedate']); ?>
            <?php print $duedatemodul->format('d-m-Y') ?>
          </div>
    <?php  } ?>
        </div>
  </div>
</div>


<div class="box-header">
  <h3 class="box-title">To Do List Of The Week</h3>
</div>
<table class='main' repeat_header="1" cellspacing="0" width="100%" style="width:100%">
  <thead>
    <tr class="head">
      <td align='center'>No</td>
      <td align='center'>To Do List</td>
      <td align='center'>Due Date</td>
      <td align='center'>Finish</td>
      <td align='center'>Status</td>
      <td align='center'>Keterangan</td>
    </tr>
  </thead>
 
  <tbody>
  <?php
    $no = 1;
    foreach ($list as $row){ ?>
    <tr>
      <td align="center" ><?php print $no++; ?></td>
      <td><?php print $row['kerjamodullist_judul']; ?></td>
      <td align="center" >
        <?php
          foreach ($row['data_duedate'] as $value)
          {
            print tanggal($value['kerjamodullistduedate_duedate'])."<br>";
            
          }?>
      </td>
      <td align="center" ><?php print tanggal($row['kerjamodullist_finish']) ?> </td>
      <td align="center" >
        <div class="selisih">
            <?php
              foreach ($row['selisih'] as $value)
              {
                print $value['selisih'];
              }
            ?>
        </div>
      </td>
      <td><?php print $row['kerjamodul_keterangan']; ?> </td>
  </tr>
  <?php } ?>
  </tbody>
</table>
<?php if (! empty($listterlambat)) { ?>
<div class="box-header">
  <h3 class="box-title">Revisi Due Date For Next Week</h3>
</div>

<table class='main' repeat_header="1" cellspacing="0" width="100%" style="width:100%">
  <thead>
    <tr class="head">
      <td align='center'>No</td>
      <td align='center'>To Do List</td>
      <td align='center'>Due Date</td>
      <td align='center'>Finish</td>
      <td align='center'>Status</td>
      <td align='center'>Keterangan</td>
    </tr>
  </thead>
 
  <tbody>
  <?php
    $no = 1;
    foreach ($listterlambat as $row){ ?>
    <tr>
      <td align="center" ><?php print $no++; ?></td>
      <td><?php print @$row['kerjamodullist_judul']; ?></td>
      <td>
        <?php
          foreach ($row['data_duedate'] as $value)
          {
            print tanggal($value['kerjamodullistduedate_duedate'])."<br>";
          }
        ?>
      </td>
      <td><?php if (is_null($row['kerjamodullist_finish'])) {print "-"; } ?></td>
      <td>-</td>

      <td class="" ><?php print @$row['kerjamodul_keterangan']; ?></td>
    </tr>
  </tr>
  
  <?php }
  }
  ?>
  </tbody>
</table>
 
<?php
$content = ob_get_clean();
 
$header =
'
  <div class= "judul">
    <b>WEEKLY</b>REPORT
  </div>
';
 
$footer = '<table cellpadding=0 cellspacing=0 style="border:none;">
           <tr><td style="margin-right:-5px;border:none;" align="left">
           Dicetak: '.date("Y-m-d H:i").'</td>
           <td style="margin-right:-5px;border:none;" align="right">
           Halaman: {PAGENO} / {nb}</td></tr></table>';            
 
try {
    $mpdf=new mPDF('utf-8', "A4", 9 ,'Arial', 5, 5, 20, 5, 5, 4);
    $mpdf->setHTMLHeader($header);
    $mpdf->setHTMLFooter($footer);
    $mpdf->WriteHTML($content);
    $mpdf->Output("laporan-stok.pdf","I");
} catch(Exception $e) {
    echo $e;
    exit;
}
?>