<!DOCTYPE html>
<html>
<head>
  <title><?php print $judul; ?> | manajemen Kerja</title>
  <?php $this->load->view('head'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <?php $this->load->view('profile_view'); ?>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
  <?php $this->load->view('menu_view');?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php print $judul; ?>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php print site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php print site_url('modul') ?>">Manajemen Kerja</a></li>
        <li class="active">Modul Kerja</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <a href="" id="btn-refresh"><button type="button" class="btn btn-success"><i class="fa fa-refresh"></i></button></a>

          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
       <form method="post" action="<?php print site_url("modul/update_simpan_list/".$modul['kerjamodul_id']); ?>">
          <div class="box-body">
            <div class="row">
            <input name="modulid" type="hidden" value= "<?php print $modul['kerjamodul_id']; ?>" ></input>
              <div class="col-md-3">

              <div class="row">
                <div class="col-sm-12">
                  <!-- Progress bars -->
                  <div class="clearfix">
                    <span class="pull-left">Pencapaian</span>
                    <?php
                      $pencapaianall = count($pencapaian);
                      $pencapaianselesai = count($pencapaianselesai);
                      $pencapaian = ($pencapaianselesai/$pencapaianall)*100;
                    ?>
                      <small class="pull-right"><?php print round($pencapaian, 1)."%"; ?></small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: <?php print $pencapaian."%"; ?>;"></div>
                  </div>                  
                </div>

                <!-- /.col -->

              </div>
              <div id="statusmodul" class="form-group">
                <select id="statusmodul" class="form-control select2" data-placeholder="Pilih Status" style="width: 100%;" name="status" id="status">
                    <?php
                      foreach ($status_modul as $row)
                      {
                        if(($modul['kerjamodul_statusid']) == ($row['status_id']))
                        {
                          ?>
                          <option selected="selected" value="<?php print $row['status_id'];?>"><?php print $row['status_nama'];?></option>
                        <?php }
                      else
                      {
                        ?>
                        <option value="<?php print $row['status_id'];?>"><?php print $row['status_nama'];?></option>
                      <?php }
                      
                    } ?>
                  </select>
                </div>

                <div class="form-group">
                  <input disabled type="text" class="form-control" id="judul" placeholder="Judul" name = "judul" value= "<?php print $modul['kerja_judul']; ?>" >
                </div>

                <div class="form-group">
                  <input disabled type="text" class="form-control" id="judul" placeholder="Judul" name = "judul" value= "<?php print $modul['kerjamodul_judul']; ?>" >
                </div>

                <div class="form-group">
                  <textarea disabled class="form-control" rows="3" placeholder="Keterangan" name = "keterangan"><?php print $modul['kerjamodul_keterangan']; ?> </textarea>
                </div>

                <div class="form-group">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input disabled type="text" class="form-control pull-right" id="startdate" placeholder="Start Date" name="startdate" value= "<?php print $modul['startdate'] ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input disabled type="text" class="form-control pull-right" id="duedate" placeholder="Due Date" name="duedate" value= "<?php print $modul['duedate'] ?>" >
                    </div>
                  </div>
                  <div class="form-group">
                    <input disabled type="text" class="form-control" id="judul" placeholder="Judul" name = "judul" value= "<?php print $modul['kerjamodul_judul']; ?>" >
                  </div>
                  <div class="form-group">
                    <input disabled type="text" class="form-control" id="judul" placeholder="Judul" name = "judul" value= "<?php print $modul['manajemenkerja_user_nama']; ?>" >
                  </div>
              </div>

              <div class="col-md-4">
                <div id="task-list">
                  <div class="form-group">
                    <select disabled class="form-control select2" multiple="multiple" data-placeholder="Pilih Anggota" style="width: 100%;" name="anggota[]" id="anggota">
                      <?php foreach ($user as $row): 

                      $strSelected = in_array($row['manajemenkerja_user_id'], $modul['modul_member_id']) ? "selected='selected'" : "";

                      ?>
                        <option value="<?php echo $row['manajemenkerja_user_id']; ?>" <?php echo $strSelected; ?>>
                          <?php echo $row['manajemenkerja_user_nama']; ?>
                        </option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <?php
                  foreach ($list as $row)
                  {
                    if  ($row['kerjamodullist_penyelesaianid'] == '2')
                    {?>
                      <div class="form-group">
                          <table class="table table-bordered table-striped"><td><input height="30px" id="status" name="list[]" type="checkbox" value="<?php print $row['kerjamodullist_id']; ?>" class="minimal" checked></td>
                            <td><label><?php print $row['kerjamodullist_judul']; ?></label></td></table>
                       
                      </div>
                      <?php }
                      else if ($row['kerjamodullist_penyelesaianid'] == '1')
                      {?>
                      <div class="form-group">
                        <label>
                          <table  class="table table-bordered table-striped"><td><input id = "status" name="list[]" type="checkbox" value="<?php print $row['kerjamodullist_id']; ?>" class="minimal"></td>
                            <td><label><?php print $row['kerjamodullist_judul']; ?></label></td></table>  
                      </div>
                    <?php }}
                  ?>
                </div>
              </div>

              <div class="col-md-5">

        
                <div class="input-group input-group-lg">
                </div>
                <div class="input-group input-group-sm">
                  <input name="komentar" placeholder="komentar" type="text" class="form-control">
                      <span class="input-group-btn">
                        <button id="simpankomentar" type="button" class="btn btn-info btn-flat">Simpan</button>
                      </span>
                </div>
                </br>

              
              <!-- /input-group -->
            

                <!-- The time line -->
                <ul class="timeline">
                  <!-- timeline time label -->
                  <?php foreach ($timeline as $row) {?>
                    
                  <li class="time-label">
                        <span class="bg-red">
                          <?php print $row['tanggal'] ?>
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa <?php print $row['timeline_simbol'] ?> bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> <?php print $row['waktu'] ?></span>

                      <h3 class="timeline-header"><a href="#"><?php print $row['manajemenkerja_user_nama'] ?></a> <?php print $row['timeline_judul'] ?></h3>

                      <div class="timeline-body">
                        <?php print $row['timeline_keterangan'] ?>
                      </div>
                      <div class="timeline-footer">
                      </div>
                    </div>
                  </li>
                  <?php } ?>
                </ul>
              </div>
          </div>
        </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  <?php $this->load->view('footer'); ?>
  </footer>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>

<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#duedate, #startdate').datepicker({
      dateFormat: 'dd/mm/yyyy',
      autoclose: true
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });


  $("#simpankomentar").on('click', function()
  {
    var komentar = $(this).parents('.input-group').find('input').val();
    var modulid = $("input[name='modulid']").val();
    console.log(komentar);
    var url = "<?php echo site_url('modul/komentar_insert/'); ?>";
      $.ajax
          ({
            type: "POST",
            url: url,
            data: {id : modulid, komentar : komentar },
            success: function(res)
              {
                $("#btn-refresh")[0].click();
                console.log(res);
              },
              dataType: "JSON"
          });
  })
  
  $("#task-list").on('change',"#status", function(){
    var isChecked = $(this).is(':checked');
    var Id = $(this).val();
    if (isChecked) {
      var status = "tambah";
    }else{
      var status = "hapus";
    }
    swal
      (
        {
          title: "Are you sure?",
          text: "",
          type: "info",
          showCancelButton: true,
          // confirmButtonColor: "blue",
          confirmButtonText: "Yes!",
          // cancelButtonText: "No!",
          // closeOnConfirm: false,
          showLoaderOnConfirm: true
          // closeOnCancel: false
        },


        function(isConfirm)
          {
            
            if (isConfirm)
            {
              var url = "<?php echo site_url('modul/updateListById/'); ?>";
              $.ajax
                  ({
                    type: "POST",
                    url: url,
                    data: {id : Id, status : status },
                    success: function(res)
                      {
                        $("#btn-refresh")[0].click();
                        console.log(res);
                      },
                      dataType: "JSON"
                  });
            }
            else
            {
              swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
          }
      );
  });

  $("#statusmodul").on('change',"#statusmodul", function(){
    var status = $(this).val();
    var statusnama = $("#statusmodul option:selected").text();
    var modulid = $("input[name='modulid']").val();
    swal
      (
        {
          title: "Are you sure?",
          text: "",
          type: "info",
          showCancelButton: true,
          // confirmButtonColor: "blue",
          confirmButtonText: "Yes!",
          // cancelButtonText: "No!",
          // closeOnConfirm: false,
          showLoaderOnConfirm: true
          // closeOnCancel: false
        },
        function(isConfirm)
          {
            
            if (isConfirm)
            {
              var url = "<?php echo site_url('modul/update_status_modul/'); ?>";
              $.ajax
                  ({
                    type: "POST",
                    url: url,
                    data: {statusnama : statusnama, status : status, modulid : modulid },
                    success: function(res)
                      {
                        $("#btn-refresh")[0].click();
                        console.log(res);
                      },
                      dataType: "JSON"
                  });
            }
            else
            {
              swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
          }
      );
  });
</script>
</body>
</html>
