-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 21 Jun 2017 pada 07.29
-- Versi Server: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pos_buda`
--

CREATE TABLE `pos_buda` (
  `pos_buda_id` int(11) NOT NULL,
  `pos_buda_username` varchar(255) NOT NULL,
  `pos_buda_email` varchar(255) NOT NULL,
  `pos_buda_pw` varchar(255) NOT NULL,
  `pos_buda_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pos_buda_delete` int(2) NOT NULL,
  `pos_budak_createby` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pos_buda`
--

INSERT INTO `pos_buda` (`pos_buda_id`, `pos_buda_username`, `pos_buda_email`, `pos_buda_pw`, `pos_buda_create`, `pos_buda_delete`, `pos_budak_createby`) VALUES
(1, 'fianprasetyo', 'fianprasetyo11@gmail.com', 'fianprasetyo', '2017-05-20 08:05:32', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pos_buda`
--
ALTER TABLE `pos_buda`
  ADD PRIMARY KEY (`pos_buda_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pos_buda`
--
ALTER TABLE `pos_buda`
  MODIFY `pos_buda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
